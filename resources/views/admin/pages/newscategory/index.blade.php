@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create NewsCategory'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.newscategories.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            NewsCategory
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>NewsCategories</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable newscategory-empty {{$newscategories->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No NewsCategories found.
        </div>
        <div class="table-responsive {{$newscategories->count() == 0 ? 'johnCena' : '' }}">
            <table id="newscategories-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    {{-- <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Content
                    </th> --}}
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($newscategories as $newscategory)
                    <tr data-newscategory-id="{{$newscategory->id}}">
                        <td class="text-center"><strong>{{ $newscategory->id }}</strong></td>
                        <td class="text-center"><strong>{{ $newscategory->name }}</strong></td>
                        {{-- <td class="text-left">
                            @if($newscategory->slug && $newscategory->slug != '')
                                <a target="_blank" href="{{ add_http($newscategory->slug) }}">{{ add_http($newscategory->slug) }}</a>
                            @endif
                        </td> --}}
                        {{-- <td class="text-left">{!! str_limit(strip_tags($newscategory->content), 50) !!}</td> --}}
                        <td class="text-center">{{ $newscategory->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read NewsCategory'))
                                    {{-- <a href="{{ route('admin.newscategories.show', $newscategory->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a> --}}
                                @endif
                                @if (auth()->user()->can('Update NewsCategory'))
                                    <a href="{{ route('admin.newscategories.edit', $newscategory->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete NewsCategory'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-newscategory-btn"
                                       data-original-title="Delete"
                                       data-newscategory-id="{{ $newscategory->id }}"
                                       data-newscategory-route="{{ route('admin.newscategories.delete', $newscategory->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/newscategories.js') }}"></script>
@endpush