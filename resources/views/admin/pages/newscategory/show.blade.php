@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.newscategories.index') }}">NewsCategories</a></li>
        <li><span href="javascript:void(0)">View NewsCategory</span></li>
    </ul>
    <div class="content-header">
        <div class="header-section">
            <h1>{{ $newscategory->name }}</h1>
            <h5>{{ $newscategory->slug }}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block block-alt-noborder">
                <article>
                    <h3 class="sub-header text-center"><strong> {{ $newscategory->created_at->format('F d, Y') }} </strong>
                        <div class="btn-group btn-group-xs pull-right">
                            @if (auth()->user()->can('Update NewsCategory'))
                                <a href="{{ route('admin.newscategories.edit', $newscategory->id) }}"
                                   data-toggle="tooltip"
                                   title=""
                                   class="btn btn-default"
                                   data-original-title="Edit"><i class="fa fa-pencil"></i> Edit</a>
                            @endif
                            @if (auth()->user()->can('Delete NewsCategory'))
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                   title=""
                                   class="btn btn-xs btn-danger delete-newscategory-btn"
                                   data-original-title="Delete"
                                   data-newscategory-id="{{ $newscategory->id }}"
                                   data-newscategory-route="{{ route('admin.newscategories.delete', $newscategory->id) }}">
                                    <i class="fa fa-times"> Delete</i>
                                </a>
                            @endif
                        </div>
                    </h3>

                    <img src="{{ asset($newscategory->banner_image) }}" alt="{{ $newscategory->banner_image }}" class="img-responsive center-block" style="max-width: 100px;">

                    <p>{!! $newscategory->content !!}</p>
                </article>
            </div>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/newscategories.js') }}"></script>
@endpush