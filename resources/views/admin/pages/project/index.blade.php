@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Project'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.projects.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Project
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>Projects</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable project-empty {{$projects->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Projects found.
        </div>
        <div class="table-responsive {{$projects->count() == 0 ? 'johnCena' : '' }}">
            <table id="projects-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        Order
                    </th>
                    <th class="text-center">
                        
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Is Active
                    </th>
                    <th class="text-left">
                        Content
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $index=>$project)
                    <tr class="project-class" data-project-order="{{$index+1}}" data-project-id="{{$project->id}}">
                        <td class="text-center counter_order">{{ $project->order }}</td>
                        <td style="cursor: move" class="text-center grabable"><strong><i class="fa fa-arrows-v"></i></strong></td>
                        <td class="text-center"><strong>{{ $project->name }}</strong></td>
                        <td class="text-left">
                            @if($project->slug && $project->slug != '')
                                <a target="_blank" href="{{ url('project-details/'.$project->slug) }}">{{ url('project-details/'.$project->slug) }}</a>
                            @endif
                        </td>
                        <!-- <td class="text-left">
                            @if($project->slug && $project->slug != '')
                                <a target="_blank" href="{{ add_http($project->slug) }}">{{ add_http($project->slug) }}</a>
                            @endif
                        </td> -->
                        <td class="text-center"><strong>{{ $project->is_active == '1' ? 'Active' : 'Inactive' }}</strong></td>
                        <td class="text-left">{!! str_limit(strip_tags($project->content), 50) !!}</td>
                        <td class="text-center">{{ $project->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                <!-- @if (auth()->user()->can('Read Project'))
                                    <a href="{{ route('admin.projects.show', $project->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a>
                                @endif -->
                                @if (auth()->user()->can('Update Project'))
                                    <a href="{{ route('admin.projects.edit', $project->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Project'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-project-btn"
                                       data-original-title="Delete"
                                       data-project-id="{{ $project->id }}"
                                       data-project-route="{{ route('admin.projects.delete', $project->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/projects.js') }}"></script>
    <script>
        $('#projects-table tbody .grabable').mousedown(function (e) {
          var tr = $(e.target).closest('tr'), sy = e.pageY, drag;
          if ($(e.target).is('tr')) tr = $(e.target);
          var index = tr.index();
          $(tr).addClass('grabbed');
          function move (e) {
            if (!drag && Math.abs(e.pageY - sy) < 10) return;
            drag = true;
            tr.siblings().each(function() {
              var s = $(this), i = s.index(), y = s.offset().top;
              if (e.pageY >= y && e.pageY < y + s.outerHeight()) {
                if (i < tr.index()) s.insertAfter(tr);
                else s.insertBefore(tr);
                return false;
              }
            });
          }
          function up (e) {
            if (drag && index != tr.index()) {
              drag = false;
            }
            $(document).unbind('mousemove', move).unbind('mouseup', up);
            $(tr).removeClass('grabbed');

            var array_project = [];

            $('.project-class').each(function()
            {
                // $(this).attr('data-project-id');
                array_project.push($(this).attr('data-project-id'));
            });

            $.ajax({
                'url' : '{{url('re_order_proeject')}}',
                'type' : 'get',
                'data' : {
                    'data' : array_project
                },
                success:function(data)
                {
                    var count = 1;
                    $('.counter_order').each(function()
                    {
                        $(this).html(''+count+'');
                        count++;
                    })
                }    
            })

          }
          $(document).mousemove(move).mouseup(up);
        });
    </script>
@endpush