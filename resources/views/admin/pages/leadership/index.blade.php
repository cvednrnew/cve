@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Leadership'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.leaderships.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Leadership
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-newspaper-o sidebar-nav-icon"></i>
                <strong>Leaderships</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable leadership-empty {{$leaderships->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Leaderships found.
        </div>
        <div class="table-responsive {{$leaderships->count() == 0 ? 'johnCena' : '' }}">
            <table id="leaderships-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-left">
                        Slug
                    </th>
                    <th class="text-left">
                        Position
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($leaderships as $leadership)
                    <tr data-leadership-id="{{$leadership->id}}">
                        <td class="text-center"><strong>{{ $leadership->id }}</strong></td>
                        <td class="text-center"><strong>{{ $leadership->name }}</strong></td>
                        <td class="text-left">
                            @if($leadership->slug && $leadership->slug != '')
                                <a target="_blank" href="{{ url('team-details/'.$leadership->slug) }}">{{ url('team-details/'.$leadership->slug) }}</a>
                            @endif
                        </td>
                        <td class="text-left">{!! str_limit(strip_tags($leadership->position), 50) !!}</td>
                        <td class="text-center">{{ $leadership->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read Leadership'))
                                    {{-- <a href="{{ route('admin.leaderships.show', $leadership->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View"><i class="fa fa-eye"></i></a> --}}
                                @endif
                                @if (auth()->user()->can('Update Leadership'))
                                    <a href="{{ route('admin.leaderships.edit', $leadership->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Leadership'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-leadership-btn"
                                       data-original-title="Delete"
                                       data-leadership-id="{{ $leadership->id }}"
                                       data-leadership-route="{{ route('admin.leaderships.delete', $leadership->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/leaderships.js') }}"></script>
@endpush