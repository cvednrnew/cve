@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.leaderships.index') }}">Leaderships</a></li>
        <li><span href="javascript:void(0)">View Leadership</span></li>
    </ul>
    <div class="content-header">
        <div class="header-section">
            <h1>{{ $leadership->name }}</h1>
            <h5>{{ $leadership->slug }}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block block-alt-noborder">
                <article>
                    <h3 class="sub-header text-center"><strong> {{ $leadership->created_at->format('F d, Y') }} </strong>
                        <div class="btn-group btn-group-xs pull-right">
                            @if (auth()->user()->can('Update Leadership'))
                                <a href="{{ route('admin.leaderships.edit', $leadership->id) }}"
                                   data-toggle="tooltip"
                                   title=""
                                   class="btn btn-default"
                                   data-original-title="Edit"><i class="fa fa-pencil"></i> Edit</a>
                            @endif
                            @if (auth()->user()->can('Delete Leadership'))
                                <a href="javascript:void(0)" data-toggle="tooltip"
                                   title=""
                                   class="btn btn-xs btn-danger delete-leadership-btn"
                                   data-original-title="Delete"
                                   data-leadership-id="{{ $leadership->id }}"
                                   data-leadership-route="{{ route('admin.leaderships.delete', $leadership->id) }}">
                                    <i class="fa fa-times"> Delete</i>
                                </a>
                            @endif
                        </div>
                    </h3>

                    <img src="{{ asset($leadership->banner_image) }}" alt="{{ $leadership->banner_image }}" class="img-responsive center-block" style="max-width: 100px;">

                    <p>{!! $leadership->content !!}</p>
                </article>
            </div>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/leaderships.js') }}"></script>
@endpush