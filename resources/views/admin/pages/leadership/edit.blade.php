@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.leaderships.index') }}">Leaderships</a></li>
        <li><span href="javascript:void(0)">Edit Leadership</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-leadership',
            'route' => ['admin.leaderships.update', $leadership->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Leadership "{{$leadership->name}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="leadership_name" name="name"
                               value="{{  Request::old('name') ? : $leadership->name }}"
                               placeholder="Enter Leadership name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_slug">Slug</label>

                    <div class="col-md-9">
                        <input disabled type="text" class="form-control" id="leadership_slug" name="slug"
                               value="{{  Request::old('slug') ? : $leadership->slug }}"
                               placeholder="Enter Leadership slug..">
                        @if($errors->has('slug'))
                            <span class="help-block animation-slideDown">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_banner_image">Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="banner_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ asset($leadership->banner_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ $leadership->banner_image != '' ? asset($leadership->banner_image) : '' }}"
                                 alt="{{ $leadership->banner_image != '' ? asset($leadership->banner_image) : '' }}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn"
                           style="display: {{ $leadership->banner_image != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_banner_image" class="remove-image" value="0">
                    </div>
                </div>

                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_position">Position</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="leadership_position" name="position"
                               value="{{  Request::old('position') ? : $leadership->position }}"
                               placeholder="Enter Leadership position..">
                        @if($errors->has('position'))
                            <span class="help-block animation-slideDown">{{ $errors->first('position') }}</span>
                        @endif
                    </div>
                </div>

                {{-- <div class="form-group file-container {{ $errors->has('file') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_file">File</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="file" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('file'))
                            <span class="help-block animation-slideDown">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a target="_blank" href="{{ asset($leadership->file) }}" class="file-anchor">
                            {{ $leadership->file }}
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-file-btn"
                           style="display: {{ $leadership->file != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_file" class="remove-file" value="0">
                    </div>
                </div> --}}
                <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_bio">Bio</label>

                    <div class="col-md-9">
                    <textarea id="leadership_bio" name="bio" rows="9" class="form-control ckeditor"
                              placeholder="Enter Leadership bio..">{!! Request::old('bio') ? : $leadership->bio !!}</textarea>
                        @if($errors->has('bio'))
                            <span class="help-block animation-slideDown">{{ $errors->first('bio') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="leadership_info">Info</label>

                    <div class="col-md-9">
                    <textarea id="leadership_info" name="info" rows="9" class="form-control ckeditor"
                              placeholder="Enter Leadership info..">{!! Request::old('info') ? : $leadership->info !!}</textarea>
                        @if($errors->has('info'))
                            <span class="help-block animation-slideDown">{{ $errors->first('info') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($leadership->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.leaderships.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/leaderships.js') }}"></script>
@endpush