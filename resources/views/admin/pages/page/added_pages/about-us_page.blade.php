
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Banner Image',2,$page->id,'public/uploads/page_section_images/about_bg-1616192301.jpg',1)  }}
{{  addSection('Title',3,$page->id,'ABOUT US',2)  }}
{{  addSection('Section 1 Left Title',3,$page->id,'WHO WE ARE',3)  }}
{{  addSection('Section 1 Heading',4,$page->id,'A HISTORY OF EXCELLENCE UNITING TRADITIONAL VALUES AND TODAY’S TECHNOLOGY',4)  }}
{{  addSection('Section 1 Content',1,$page->id,'<p>Since 1925, family-owned Chula Vista Electric (CVE) has been providing electrical services to Southern California. We started in downtown San Diego’s commercial district, in a small street-side store. As demand for electrical services grew, so did we. Today, we’re in a state-of-the-art-building in Santee, with nearly 100 employees and a fleet of service trucks. Over the years, CVE has worked in many exciting places,from mines in Chile to the sunny shores of Hawaii — designing and building electric systems for the long haul.</p>

<p>As we move into the future, CVE is embracing green and other evolving technologies and continuing to focus on being a company that exceeds industry standards and client expectations.</p>

<p>Sincerely,</p>
<img alt="Signature" class="signature" src="http://44.224.67.159//cve/public/img/signature.png" />
<p class="position">President</p>',5)  }}
{{  addSection('Section 1 Image 1',2,$page->id,'public/uploads/page_section_images/about-img_2-1616192301.jpg',6)  }}
{{  addSection('Section 1 Image 2',2,$page->id,'public/uploads/page_section_images/about-img_1-1616192301.jpg',7)  }}

{{  addSection('Section 2 Timeline Background',2,$page->id,'public/uploads/page_section_images/about-timeline-bg-1616192918.jpg',8)  }}
{{  addSection('Section 2 Timeline 1',2,$page->id,'public/uploads/page_section_images/about-timeline-img-1616192532.png',9)  }}
{{  addSection('Section 2 Timeline 2',2,$page->id,'public/uploads/page_section_images/about-timeline-img-1616192532.png',10)  }}
{{  addSection('Section 2 Timeline 3',2,$page->id,'public/uploads/page_section_images/about-timeline-img-1616192532.png',11)  }}

{{  addSection('Section 3 Right Title',3,$page->id,'SAFETY FIRST',12)  }}
{{  addSection('Section 3 Image 1',2,$page->id,'public/uploads/page_section_images/about_employee-safety-img-1616192301.jpg',13)  }}
{{  addSection('Section 3 Image 2',2,$page->id,'public/uploads/page_section_images/about_employee-safety-img2-1616192301.jpg',14)  }}
{{  addSection('Section 3 Heading',4,$page->id,'KEEPING OUR EMPLOYEES SAFE IS OUR TOP PRIORTY',15)  }}
{{  addSection('Section 3 Content',1,$page->id,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
<a class="btn btn--primary" href="team">Meet Our Team</a>',16)  }}




