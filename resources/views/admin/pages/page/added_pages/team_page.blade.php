
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Banner Image',2,$page->id,'public/img/background/team-banner_bg.jpg',1)  }}

{{  addSection('Title',3,$page->id,'Our Team',2)  }}

{{  addSection('Section 1 Left Title',3,$page->id,'Experienced Staff',4)  }}

{{  addSection('Section 1 Content',1,$page->id,'
  <h2>power for companies, powered by people.</h2>
                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit dolor sup</h4>
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                                <a href="contact-us" class="btn btn--primary">Join Our Team!</a>',5)  }}

