
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Banner Image',2,$page->id,'public/uploads/page_section_images/services_bg-1619460193.jpg',1)  }}

{{  addSection('Title',3,$page->id,'SERVICES',2)  }}

{{  addSection('Section 1 Left Title',3,$page->id,'OUR SERVICES',4)  }}

{{  addSection('Section 1 Text',3,$page->id,'Your Power Partner From Routine to Complex.',5)  }}

{{  addSection('Section 1 Col 1 Content',1,$page->id,'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit..</p>
',6)  }}

{{  addSection('Section 1 Col 2 Content',1,$page->id,'<h4>Safety First. That’s where we start. </h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam, quis nostrud exercitation ullamco laboris. nisi ut aliquip ex ea commodo consequat.. </p>
<a href="about-us" class="btn btn--primary">Learn More</a>',7)  }}

{{  addSection('Section 2 Get An Estimate Background',2,$page->id,'public/img/services/service-contact.jpg',8)  }}

{{  addSection('Section 2 Get An Estimate Content',1,$page->id,' <strong class="heading">Contact Us</strong>
<h3>Get An estimate</h3>
<p>Our expert team is ready to respond to your repair, service or new construction need. How can we help?</p>
<a href="contact-us" class="btn btn--primary btn--white btn-contact-us">
    <img src="/public/img/phone_icon_colored.png" alt="Phone Icon">
    Call Us
</a>
<a href="mailto:contactus@c-v-e.com" class="btn btn--primary btn--white btn-email-us">
    <img src="/public/img/email_icon-colored.png" alt="Email Icon">
    Email Us
</a>',9)  }}