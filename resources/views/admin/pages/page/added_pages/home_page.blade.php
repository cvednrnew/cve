
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{-- {{  addSection('Service 1 title',3,1,'',1)  }}
{{  addSection('Service 1 description',3,1,'',2)  }}

{{  addSection('Service 2 title',3,1,'',3)  }}
{{  addSection('Service 2 description',3,1,'',4)  }}

{{  addSection('Service 3 title',3,1,'',5)  }}
{{  addSection('Service 3 description',3,1,'',6)  }}

{{  addSection('Service 4 title',3,1,'',7)  }}
{{  addSection('Service 4 description',3,1,'',8)  }}

{{  addSection('Service 5 title',3,1,'',9)  }}
{{  addSection('Service 5 description',3,1,'',10)  }}

{{  addSection('Service 6 title',3,1,'',11)  }}
{{  addSection('Service 6 description',3,1,'',12)  }} --}}

{{  addSection('Section 1 Left Label',3,1,'ABOUT US',13)  }}

{{  addSection('Section 1 Content Title',3,1,'EXPERTISE THROUGH THE DECADES. INNOVATION EVERY DAY.',14)  }}

{{  addSection('Section 1 Content 1 Description',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate',15)  }}

{{  addSection('Section 1 Content 1 Image',2,1,'public/uploads/page_section_images/about-left-img-1614745457.jpg',16)  }}

{{  addSection('Section 1 Content 1 Label Bottom',3,1,'1932',17)  }}

{{  addSection('Section 1 Content 2 Description',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate',18)  }}

{{  addSection('Section 1 Content 2 Image',2,1,'public/uploads/page_section_images/about-right-img-1614745457.jpg',19)  }}

{{  addSection('Section 1 Content 2 Label Bottom',3,1,'TODAY',20)  }}

{{  addSection('Section 1 Content 2 Button 1 Text',3,1,'Learn More About Us',21)  }}

{{  addSection('Section 1 Content 2 Button 1 Link',3,1,'#',22)  }}

{{  addSection('Section 1 Content 2 Button 2 Text',3,1,'View Our Services',23)  }}

{{  addSection('Section 1 Content 2 Button 2 Link',3,1,'#',24)  }}

{{  addSection('Section 2 Title',3,1,'OUR PROJECTS',25)  }}

{{  addSection('Section 3 Testimonial Label',3,1,'TESTIMONIALS',26)  }}

{{  addSection('Section 3 Testimonial Saying',3,1,'WHAT OUR CLIENTS ARE SAYING',27)  }}

{{  addSection('Section 3 Testimonial 1 Content',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',28)  }}

{{  addSection('Section 3 Testimonial 1 Name',3,1,'John Williams',29)  }}

{{  addSection('Section 3 Testimonial 1 Description',3,1,'Chula Vista Mall',30)  }}

{{  addSection('Section 3 Testimonial 2 Content',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 1',31)  }}

{{  addSection('Section 3 Testimonial 2 Name',3,1,'John Williams',32)  }}

{{  addSection('Section 3 Testimonial 2 Description',3,1,'Chula Vista Mall',33)  }}

{{  addSection('Section 3 Testimonial 3 Content',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 2',34)  }}

{{  addSection('Section 3 Testimonial 3 Name',3,1,'John Williams',35)  }}

{{  addSection('Section 3 Testimonial 3 Description',3,1,'Chula Vista Mall',36)  }}

{{  addSection('Section 3 Testimonial 4 Content',4,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 3',37)  }}

{{  addSection('Section 3 Testimonial 4 Name',3,1,'John Williams',38)  }}

{{  addSection('Section 3 Testimonial 4 Description',3,1,'Chula Vista Mall',39)  }}

{{  addSection('Section 4 Company 1 Image',2,1,'public/uploads/page_section_images/client-1-1614745457.png',40)  }}

{{  addSection('Section 4 Company 2 Image',2,1,'public/uploads/page_section_images/client-2-1614745457.png',41)  }}

{{  addSection('Section 4 Company 3 Image',2,1,'public/uploads/page_section_images/client-3-1614745457.png',42)  }}

{{  addSection('Section 4 Company 4 Image',2,1,'public/uploads/page_section_images/client-4-1614746058.png',43)  }}

{{  addSection('Section 4 Company 5 Image',2,1,'public/uploads/page_section_images/client-5-1614745457.png',44)  }}
