
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Banner Image',2,3,'public/uploads/page_section_images/contact_bg-1614751312.jpg',1)  }}

{{  addSection('Title',3,3,'CONTACT US',2)  }}

{{  addSection('Section 1 Left Title',3,3,'CONTACT INFO',3)  }}

{{  addSection('Section 1 Description',4,3,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate',4)  }}

{{  addSection('Section 2 Form Title',3,3,'CONTACT US',5)  }}

{{  addSection('Section iFrame',4,3,'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3351.9897984363447!2d-116.96360678449186!3d32.845521780951465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dbe29ff4712b0b%3A0xbc28748b99c7571e!2s9344%20Wheatlands%20Rd%2C%20Santee%2C%20CA%2092071%2C%20USA!5e0!3m2!1sen!2sph!4v1614666637143!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',6)  }}

