
{{--
    add section info
    
    addSection($name, $type, $pages, $value = '', $pos)
    Type EDITOR = 1;
    Type ATTACHMENT = 2;
    Type FORM = 3;
    Type TEXTAREA = 4;

--}}

{{  addSection('Banner Image',2,$page->id,'public/uploads/page_section_images/renewable-energy-banner-1619461861.jpg',1)  }}

{{  addSection('Title',3,$page->id,'RENEWABLE ENERGY',2)  }}

{{  addSection('Section 1 Left Title',3,$page->id,'GO GREEN TODAY',4)  }}

{{  addSection('Section 1 Content',1,$page->id,'
<h2>The Power of the Future — Today. </h2>
<h3>Ready to go Green?</h3>
<p>We’ve seen a lot of change since 1925, when CVE was founded. Throughout the years, we’ve evolved  to  meet  new  demands,  and  today  we  stand  ready  to  help  you  enhance  your  green  footprint in a variety of ways, including solar, wind generation, geothermal, hydroelectric, micro grid, fuel cells, and battery storage. We even have the expertise to install and maintain electric vehicle charge stations. Step into the future today with our help.</p>
',5)  }}

{{  addSection('Section 2 Content',1,$page->id,'<div class="col-lg-4">
    <div class="list-con">
    <div class="list-con--icon"><img alt="Renewable Energy" src="http://44.224.67.159/cve/public/img/renewable-energy/renewable-energy-icon-1.png" />
    <div class="title">Consult/Plan</div>
    </div>
    
    <ul>
        <li>Learn Specific Needs</li>
        <li>Define Expectations</li>
        <li>Design Your System</li>
        <li>Establish Goals</li>
    </ul>
    </div>
    </div>
    
    <div class="col-lg-4">
    <div class="list-con">
    <div class="list-con--icon"><img alt="Renewable Energy" src="http://44.224.67.159/cve/public/img/renewable-energy/renewable-energy-icon-2.png" />
    <div class="title">Install/Maintain</div>
    </div>
    
    <ul>
        <li>Plan For Safety</li>
        <li>Use Creativity</li>
        <li>Provide ROI</li>
        <li>Focus on Quantity</li>
    </ul>
    </div>
    </div>
    
    <div class="col-lg-4">
    <div class="list-con">
    <div class="list-con--icon"><img alt="Renewable Energy" src="http://44.224.67.159/cve/public/img/renewable-energy/renewable-energy-icon-3.png" />
    <div class="title">Repair/Replace</div>
    </div>
    
    <ul>
        <li>Maintain Oerations</li>
        <li>Respond Quickly</li>
        <li>Fix Critical Systems</li>
        <li>Enhance Functionality</li>
    </ul>
    </div>
    </div>
    ',6)  }}



{{  addSection('Section 3 Left Title',3,$page->id,'INNOVATION',7)  }}

{{  addSection('Section 3 Col 1 Content',1,$page->id,'
<h3>Innovative Technologies</h3>
<h4>Discover the Benefits & Challenges of Renewable Energy</h4>
<p>The area of renewable energy is quickly evolving, and we’re keeping up with the changes to ensure we can provide the   best service to you. Whether you know which clean energy alternative you prefer, or you need to learn more before deciding, we can be of help when it comes time to design and install your system, as well as maintain it.</p>
<a href="contact-us" class="btn btn--primary">Contact Us Today!</a>',8)  }}

{{  addSection('Section 3 Col 2 Image 1',2,$page->id,'public/uploads/page_section_images/renewable-energy-innovation-im-1619461950.jpg',9)  }}
{{  addSection('Section 3 Col 2 Text 1',3,$page->id,'Solar',10)  }}
{{  addSection('Section 3 Col 2 Link 1',3,$page->id,'services',11)  }}

{{  addSection('Section 3 Col 2 Image 2',2,$page->id,'public/uploads/page_section_images/renewable-energy-innovation-im-1619461926.jpg',12)  }}
{{  addSection('Section 3 Col 2 Text 2',3,$page->id,'Battery Storage System',13)  }}
{{  addSection('Section 3 Col 2 Link 2',3,$page->id,'services',14)  }}

{{  addSection('Section 3 Col 2 Image 3',2,$page->id,'public/uploads/page_section_images/renewable-energy-innovation-im-1619461862.jpg',15)  }}
{{  addSection('Section 3 Col 2 Text 3',3,$page->id,'Electric Vehicle Infrastructure',16)  }}
{{  addSection('Section 3 Col 2 Link 3',3,$page->id,'services',17)  }}

{{-- {{  addSection('Section 4 Left Title',3,$page->id,'EXPERIENCED TEAM',18)  }} --}}

{{  addSection('Section 4 Content',1,$page->id,'<div class="col-lg-6">
    <div class="overview"><strong class="heading">Experienced Team</strong>
    
    <h3>Experience that Matters</h3>
    
    <h4>Let’s Step into The Future Together</h4>
    
    <p>Our experienced team has a well-deserved reputation for excellence, so we’re excited about demonstrating our abilities in the exciting and evolving area of renewable energy. After we learn your specific needs, we’ll design/install a state-of-the-art system that will meet or exceed them.</p>
    </div>
    </div>
    
    <div class="col-lg-6">
    <div class="overview">
    <p><em>“CVE did a great job on our project it was a very professional installation. The project was well planned and executed through all phases. As always, it was a pleasure working with CVE.”</em></p>
    
    <p class="alt"><strong>John Williams</strong></p>
    
    <p class="alt"><strong>CHULA VISTA MALL</strong></p>
    </div>
    </div>
    ',19)  }}

{{  addSection('Section 5 Heading',3,$page->id,'A Few Clients We Server Are:',20)  }}

{{  addSection('Section 5 Image 1',2,$page->id,'public/uploads/page_section_images/renewable-energy-client-1-1619461862.png',21)  }}
{{  addSection('Section 5 Image 2',2,$page->id,'public/uploads/page_section_images/renewable-energy-client-2-1619461862.png',22)  }}
{{  addSection('Section 5 Image 3',2,$page->id,'public/uploads/page_section_images/renewable-energy-client-3-1619461862.png',23)  }}
{{  addSection('Section 5 Image 4',2,$page->id,'public/uploads/page_section_images/renewable-energy-client-4-1619461862.png',24)  }}
{{  addSection('Section 5 Image 5',2,$page->id,'public/uploads/page_section_images/renewable-energy-client-5-1619461862.png',25)  }}








