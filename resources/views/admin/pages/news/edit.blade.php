@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.news.index') }}">News</a></li>
        <li><span href="javascript:void(0)">Edit News</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-news',
            'route' => ['admin.news.update', $news->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit News "{{$news->name}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="news_name" name="name"
                               value="{{  Request::old('name') ? : $news->name }}"
                               placeholder="Enter News name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_slug">Slug</label>

                    <div class="col-md-9">
                        <input disabled type="text" class="form-control" id="news_slug" name="slug"
                               value="{{  Request::old('slug') ? : $news->slug }}"
                               placeholder="Enter News slug..">
                        @if($errors->has('slug'))
                            <span class="help-block animation-slideDown">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_date">Date</label>

                    <div class="col-md-9">
                        <input type="date" class="form-control" id="news_date" name="date"
                               placeholder="Enter News date.." value="{{ old('date') ? : $news->date }}">
                        @if($errors->has('date'))
                            <span class="help-block animation-slideDown">{{ $errors->first('date') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_link">Website Link</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="news_link" name="link"
                               placeholder="Enter News link.." value="{{ old('link') ? : $news->link }}">
                        @if($errors->has('link'))
                            <span class="help-block animation-slideDown">{{ $errors->first('link') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_name">Category</label>
                    <div class="col-md-9">

                        <select class="form-control" name="category_id" id="news_category_id">

                            @foreach (\App\Models\NewsCategory::all() as $item)
                                <option {{old('category_id') == $item->id ? 'selected' : ($news->category_id ==  $item->id ? 'selected' : '') }} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach

                        </select>


                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_banner_image">Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="banner_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('banner_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('banner_image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ asset($news->banner_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ $news->banner_image != '' ? asset($news->banner_image) : '' }}"
                                 alt="{{ $news->banner_image != '' ? asset($news->banner_image) : '' }}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn"
                           style="display: {{ $news->banner_image != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_banner_image" class="remove-image" value="0">
                    </div>
                </div>
                {{-- <div class="form-group file-container {{ $errors->has('file') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_file">File</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="file" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('file'))
                            <span class="help-block animation-slideDown">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a target="_blank" href="{{ asset($news->file) }}" class="file-anchor">
                            {{ $news->file }}
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-file-btn"
                           style="display: {{ $news->file != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_file" class="remove-file" value="0">
                    </div>
                </div> --}}

                <div class="form-group{{ $errors->has('content_1') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_content_1">Content</label>

                    <div class="col-md-9">
                    <textarea id="news_content_1" name="content_1" rows="9" class="form-control ckeditor"
                              placeholder="Enter News content_1..">{!! Request::old('content_1') ? : $news->content_1 !!}</textarea>
                        @if($errors->has('content_1'))
                            <span class="help-block animation-slideDown">{{ $errors->first('content_1') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('content_2') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="news_content_2">Content</label>

                    <div class="col-md-9">
                    <textarea id="news_content_2" name="content_2" rows="9" class="form-control ckeditor"
                              placeholder="Enter News content_2..">{!! Request::old('content_2') ? : $news->content_2 !!}</textarea>
                        @if($errors->has('content_2'))
                            <span class="help-block animation-slideDown">{{ $errors->first('content_2') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($news->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.news.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/news.js') }}"></script>
@endpush