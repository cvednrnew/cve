@extends('front.layouts.base')

@section('content')
    @if (!empty($page))
        <?php $item = $page; ?>
        {{-- need to be updated to live slugs or by page type --}}
        @if ($page['slug'] == 'home')
            @include('front.pages.custom-page.home')
        @elseif ($page['slug'] == 'contact-us')
            @include('front.pages.custom-page.contact-us')
        @elseif ($page['slug'] == 'about-us')
            @include('front.pages.custom-page.about-us')
        @elseif ($page['slug'] == 'news')
            @include('front.pages.custom-page.news')

        {{-- @elseif ($page['slug'] == 'news-details')
            @include('front.pages.custom-page.news-details') --}}

        @elseif ($page['slug'] == 'services')
            @include('front.pages.custom-page.services')

        {{-- @elseif ($page['slug'] == 'service-details')
            @include('front.pages.custom-page.service-details') --}}

        @elseif ($page['slug'] == 'team')
            @include('front.pages.custom-page.team')

        {{-- @elseif ($page['slug'] == 'team-details')
            @include('front.pages.custom-page.team-details') --}}

        @elseif ($page['slug'] == 'renewable-energy')
            @include('front.pages.custom-page.renewable-energy')
        @elseif ($page['slug'] == 'projects')
            @include('front.pages.custom-page.projects')

        {{-- @elseif ($page['slug'] == 'project-details')
            @include('front.pages.custom-page.project-details') --}}

        @else
            @include('front.pages.custom-page.default-page')
        @endif
    @else
        @include('front.pages.custom-page.home')
    @endif
@endsection
