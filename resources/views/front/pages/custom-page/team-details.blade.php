@extends('front.layouts.base')

@section('content')

<section class="page page--team-detail">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Team">
            {{-- <img src="{{asset('public/img/team/team-banner.jpg')}}" alt="Team"> --}}
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    {{-- <h1>Our Team</h1> --}}
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
        {{-- END OF BANNER --}}
        <div class="bg">

            {{-- Services Detail Content --}}
            <section class="section--team-overview">
                <div class="wrapper">
                    <a href="{{url('team')}}" class="btn btn--back">Back to Team</a>
                    <div class="section--team-overview--content ">
                        {{-- <strong class="heading">Our Team</strong> --}}
                        <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                        <div class="row">
                            <div class="col-lg-3 col-md-4 profile-container">
                                <img src="{{asset(''.$team->banner_image.'')}}" alt="{{$team->name}}" class="profile-img">
                                
                            </div>
                            <div class="col-lg-9 col-md-8">
                                <div class="name">{{$team->name}}</div>
                                <div class="position">{{$team->position}}</div>
                                
                                {!!$team->bio!!}

                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p> --}}
                                
                                <hr>
                                
                                <div class="personal-info">Personal Info</div>
                                    {!!$team->info!!}
                                {{-- <div class="info-box">
                                    <div class="column">
                                        <p>Education</p>
                                    </div>
                                    <div class="column">
                                        <p><cite>SDSU MBA  |  1990</cite></p>
                                    </div>
                                </div>
                                <div class="info-box">
                                    <div class="column">
                                        <p>Experience</p>
                                    </div>
                                    <div class="column">
                                        <p>1990 -  1995 :  Sonny Electronics  <cite>|  Marketer</cite></p>
                                        <p>1995 - 2010 :  GE  <cite>|  Marketing Director</cite></p>
                                        <p>2010 - Present :  CVE  <cite>|  Marketing Director</cite></p>
                                    </div>
                                </div>
                                <div class="info-box">
                                    <div class="column">
                                        <p>Skills</p>
                                    </div>
                                    <div class="column">
                                        <cite>Electronic Engineering</cite>
                                        <cite>SEO</cite>
                                    </div>
                                </div> --}}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{-- End of Services Content --}}

            {{-- Services Detail function --}}
            <section class="section--team-member">
                <div class="wrapper">
                    <div class="row heading">
                        <div class="col-md-6">
                            <h2>Other Team Members</h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <ul>
                                <li >
                                    <a href="#" class="team-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <span class="slidercount"></span>
                                </li>
                                <li>
                                    <a href="#" class="team-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-lg-3">
                            <a href="#" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-3.jpg')}}" alt="Team Member 3">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="#" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-4.jpg')}}" alt="Team Member 4">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="#" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-5.jpg')}}" alt="Team Member 5">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="#" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-6.jpg')}}" alt="Team Member 6">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                    </div> --}}
                    <div class="team-member-slider">

                        @foreach (\App\Models\Leadership::all() as $item_suggestion)
                            
                            @if($item_suggestion->id != $team->id)
                                <a href="{{url('team-details/'.$item_suggestion->slug)}}" class="team-member-box">
                                    <div class="relative">
                                        <figure>
                                            <img src="{{asset(''.$item_suggestion->banner_image.'')}}" alt="Team Member 1">
                                        </figure>
                                        <div class="overlay">
                                            <h3>{{$item_suggestion->name}}</h3>
                                            <p>{{$item_suggestion->position}}</p>
                                        </div>
                                    </div>
                                </a>
                            @endif

                        @endforeach

                        {{-- <a href="#" class="team-member-box">
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/team/team-member-3.jpg')}}" alt="Team Member 1">
                                </figure>
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="team-member-box">
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/team/team-member-4.jpg')}}" alt="Team Member 2">
                                </figure>
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="team-member-box">
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/team/team-member-5.jpg')}}" alt="Team Member 3">
                                </figure>
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="team-member-box">
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/team/team-member-6.jpg')}}" alt="Team Member 4">
                                </figure>
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="team-member-box">
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/team/team-member-7.jpg')}}" alt="Team Member 5">
                                </figure>
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </div>
                        </a> --}}
                        
                   </div>
                </div>
            </section>

            {{-- End of Services detail function --}}

        </div>

        
    </main>
    @include('front.layouts.sections.footer')
</section>

@endsection

