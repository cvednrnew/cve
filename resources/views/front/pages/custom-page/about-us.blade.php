<section class="page page--about-us">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        
         {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Contact Us">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
         {{-- END OF BANNER --}}

         <section class="section--tabs text-center">
            <div class="wrapper">
                <ul>
                    <li>
                        <a href="#whowearea" class="active">Who We Are</a>
                    </li>
                    <li>
                        <a href="#our-history">Our History</a>
                    </li>
                    <li>
                        <a href="#employee-safety">Employee Safety</a>
                    </li>
                </ul>
            </div>
         </section>

        {{-- SECTION WHO WE ARE --}}
        <section id="whowearea" class="section--who-we-are bg" style="background-image: url('{{asset('public/img/background/who-we-are_bg.jpg')}}')">
            <div class="section--who-we-are__content clearfix">
                <div class="floatLeft section--who-we-are__content-text">
                    <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                    <h2>{{section($page,'Section 1 Heading')}}</h2>
                     {{-- <h2>A history of excellence uniting traditional values and today’s technology</h2> --}}
                    {!! section($page,'Section 1 Content') !!}
                     {{-- <p>Since 1925, family-owned Chula Vista Electric (CVE) has been providing electrical services to Southern California. We started in downtown San Diego’s commercial district, in a small street-side store. As demand for electrical services grew, so did we. Today, we’re in a state-of-the-art-building in Santee, with nearly 100 employees and a fleet of service trucks. Over the years, CVE has worked in many exciting places,from mines in Chile to the sunny shores of Hawaii — designing and building electric systems for the long haul.</p>
                    <p>As we move into the future, CVE is embracing green and other evolving technologies and continuing to focus on being a company that exceeds industry standards and client expectations.</p>
                    <p>Sincerely,</p>
                    <img src="{{asset('public/img/signature.png')}}" alt="Signature" class="signature">
                    <p class="position">President</p> --}}
                </div>
                <div class="floatRight section--who-we-are__content-img">
                    {{-- <div class="img_1">
                        <img src="{{asset(''.section($page,'Section 1 Image 2').'')}}" alt="Award Winning Company You Can Trust">
                    </div> --}}
                    <div class="img_2">
                        <img src="{{asset(''.section($page,'Section 1 Image 1').'')}}" alt="Award Winning Company You Can Trust">
                    </div>
                    {{-- <div class="img_3">
                        <img src="{{asset('public/img/about-timeline-img.png')}}" alt="">
                    </div> --}}
                </div>
            </div>
        </section>
        {{-- END OF SECTION WHO WE ARE --}}

        {{-- SECTION TIMELINE SLIDER --}}

        {{-- this is the dynamic variable for timeline background --}}
        {{-- section($page,'Section 2 Timeline Background') --}}

        <section id="our-history" class="section--timeline">
            <!-- Add Arrows -->
            <div class="text-center">
                <ul>
                    <li >
                        <a href="javascript:void(0)" class="timeline-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="timeline-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
            <div class="section--timeline-slider">
                <div class="relative">
                    <img src="{{asset(''.section($page,'Section 2 Timeline 1').'')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset(''.section($page,'Section 2 Timeline 2').'')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset(''.section($page,'Section 2 Timeline 3').'')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset('public/img/timeline/slide4.png')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset('public/img/timeline/slide5.png')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset('public/img/timeline/slide6.png')}}" alt="">
                </div>
                <div class="relative">
                    <img src="{{asset('public/img/timeline/slide7.png')}}" alt="">
                </div>
            </div>
        </section>
        {{-- END OF SECTION TIMELINE SLIDER --}}

        {{-- SECTION SATEFY FIRST --}}
        <section id="employee-safety" class="section--safety-first">
            <div class="section--safety-first__content clearfix">
                <div class="floatLeft section--safety-first__content-img">
                    <div class="img_1">
                        <img src="{{asset(''.section($page,'Section 3 Image 1').'')}}" alt="Safety First">
                    </div>
                    <div class="img_2">
                        <img src="{{asset(''.section($page,'Section 3 Image 2').'')}}" alt="Safety First">
                    </div>
                </div>
                <div class="floatRight section--safety-first__content-text">
                    <strong class="heading">{{section($page,'Section 3 Right Title')}}</strong>
                    <h2>{{ section($page,'Section 3 Heading') }}</h2>
                    {{-- <h2>Keeping our Employees Safe is our Top priorty</h2> --}}

                    {!! section($page,'Section 3 Content') !!}
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    <a href="{{url('team')}}" class="btn btn--primary">Meet Our Team</a> --}}

                </div>
            </div>
        </section>
        {{-- END OF SECTION SATEFY FIRST --}}

        
    </main>
    @include('front.layouts.sections.footer')
</section>
