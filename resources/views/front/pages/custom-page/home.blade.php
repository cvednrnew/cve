<section class="homepage homepage--main">
    @include('front.layouts.sections.header')
    @include('front.pages.custom-page.sections.slider')
    <main id="main" class="main-content">
        
        {{-- SERVICES --}}
        <section class="section--services">
            <div class="list-of-services row">

                @foreach (\App\Models\Service::limit(6)->where('is_active',1)->get() as $item)
                    <div class="col-lg-2 list-of-services--item">
                        <a href="{{url('service-details/'.$item->slug)}}">
                            <div class="content">
                                <h3>{{$item->name}} <span class="line"></span></h3>
                                <p>{!! str_limit(strip_tags($item->content), 50) !!}</p>
                                <div class="text-center">
                                    <span class="btn btn--white">Learn More</span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

                {{-- <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Commercial <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Design Build <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Health Care <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Industrial <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Network Systems <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 list-of-services--item">
                    <a href="{{url('service-details')}}">
                        <div class="content">
                            <h3>Medium & High Voltage  <span class="line"></span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                            <div class="text-center">
                                <span class="btn btn--white">Learn More</span>
                            </div>
                        </div>
                    </a>
                </div> --}}

            </div>
        </section>
        {{-- END OF SERVICES --}}

        {{-- ABOUT & OUR PROJECTS--}}
        
        <section class="bg"  style="background-image: url('{{asset('public/img/about-bg.jpg')}}');">
            
            {{-- ABOUT SECTION --}}
            <section class="section--about">
                <div class="wrapper">
                    <div class="section--about-content">

                        <strong class="heading"> {{section($page,'Section 1 Left Label')}} </strong>

                        <h2> {{section($page,'Section 1 Content Title')}} </h2>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    {{section($page,'Section 1 Content 1 Description')}}
                                    {{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate--}}
                                </p> 
                                {{-- <figure class="left-image">
                                    <img src="{{asset(''.section($page,'Section 1 Content 1 Image').'')}}" alt="About Us">
                                    <span class="caption">{{section($page,'Section 1 Content 1 Label Bottom')}}</span>
                                </figure> --}}
                            </div>
                            <div class="col-md-6">
                                <p>
                                    {{section($page,'Section 1 Content 2 Description')}}
                                    {{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate --}}
                                </p>
                                {{-- <figure class="right-image">
                                    <img src="{{asset(''.section($page,'Section 1 Content 2 Image').'')}}" alt="About Us">
                                    <span class="caption">{{section($page,'Section 1 Content 2 Label Bottom')}}</span>
                                </figure>
                                <a href="{{section($page,'Section 1 Content 2 Button 1 Link')}}" class="btn btn--primary">{{section($page,'Section 1 Content 2 Button 1 Text')}}</a>
                                <a href="{{section($page,'Section 1 Content 2 Button 2 Link')}}" class="btn btn--white">{{section($page,'Section 1 Content 2 Button 2 Text')}}</a> --}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <figure class="left-image">
                                    <img src="{{asset(''.section($page,'Section 1 Content 1 Image').'')}}" alt="About Us">
                                    <span class="caption">{{section($page,'Section 1 Content 1 Label Bottom')}}</span>
                                </figure>
                            </div>
                            <div class="col-md-6">
                                <figure class="right-image">
                                    <img src="{{asset(''.section($page,'Section 1 Content 2 Image').'')}}" alt="About Us">
                                    <span class="caption">{{section($page,'Section 1 Content 2 Label Bottom')}}</span>
                                </figure>
                                <a href="{{section($page,'Section 1 Content 2 Button 1 Link')}}" class="btn btn--primary">{{section($page,'Section 1 Content 2 Button 1 Text')}}</a>
                                <a href="{{section($page,'Section 1 Content 2 Button 2 Link')}}" class="btn btn--white">{{section($page,'Section 1 Content 2 Button 2 Text')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- PROJECTS SECTION --}}
            <section class="section--projects">
                <div class="project--lists">
                    <div class="heading row clearfix">
                        <div class="col-lg-6">
                            <h2>{{section($page,'Section 2 Title')}}</h2>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" class="project-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="project-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a href="{{url('projects')}}" class="btn btn--view">View all Projects</a>
                        </div>
                    </div>
                    <ul class="project--slider">
                        @foreach (\App\Models\Project::where('is_active',1)->orderBy('order','asc')->get() as $item)
                            <li>
                                <div class="relative">
                                    <figure>
                                        <img src="{{asset(''.$item->banner_image.'')}}" alt="Project">
                                    </figure>
                                    <div class="caption">
                                        <h3><a href="{{url('project-details/'.$item->slug.'')}}"> {{$item->name}}</a></h3>
                                        <a href="{{url('project-details/'.$item->slug.'')}}" class="btn btn--learnmore">Learn More</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        {{-- <li>
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/project-1.jpg')}}" alt="Project">
                                </figure>
                                <div class="caption">
                                    <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                    <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/project-2.jpg')}}" alt="Project">
                                </figure>
                                <div class="caption">
                                    <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                    <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="relative">
                                <figure>
                                    <img src="{{asset('public/img/project-3.jpg')}}" alt="Project">
                                </figure>
                                <div class="caption">
                                    <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                    <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                                </div>
                            </div>
                        </li> --}}
                    </ul>
                </div>
            </section>
        </section>
        
        {{-- END OF ABOUT & OUR PROJECTS --}}
        
        <section class="section--client-testimonials">
            <div class="wrapper">
                <div class="clearfix row client-testimonial-heading">
                    <div class="col-lg-6">
                        <strong class="heading">{{section($page,'Section 3 Testimonial Label')}}</strong>
                        <h2>{{section($page,'Section 3 Testimonial Saying')}}</h2>
                        <!-- Add Arrows -->
                        <ul>
                            <li >
                                <a href="javascript:void(0)" class="prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <span class="slidercount"></span>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <!-- Testimonial -->
                        <div class="client-testimonail-slider">
                            <div class="slide">
                                <p>
                                    {{section($page,'Section 3 Testimonial 1 Content')}}
                                 </p>
                                <span class="author">{{section($page,'Section 3 Testimonial 1 Name')}}</span>
                                <span class="position">{{section($page,'Section 3 Testimonial 1 Description')}}</span>
                            </div>
                            <div class="slide">
                                <p>
                                    {{section($page,'Section 3 Testimonial 2 Content')}}
                                </p>
                                <span class="author">{{section($page,'Section 3 Testimonial 2 Name')}}</span>
                                <span class="position">{{section($page,'Section 3 Testimonial 2 Description')}}</span>
                            </div>
                            <div class="slide">
                                <p>
                                    {{section($page,'Section 3 Testimonial 3 Content')}}
                                 </p>
                                <span class="author">{{section($page,'Section 3 Testimonial 3 Name')}}</span>
                                <span class="position">{{section($page,'Section 3 Testimonial 3 Description')}}</span>
                            </div>
                            <div class="slide">
                                <p>
                                    {{section($page,'Section 3 Testimonial 4 Content')}}
                                 </p>
                                <span class="author">{{section($page,'Section 3 Testimonial 4 Name')}}</span>
                                <span class="position">{{section($page,'Section 3 Testimonial 4 Description')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- Clients Logo --}}
        <section class="section--clients">
            <div class="wrapper">
                <div class="text-center">
                    <h2>A Few Clients We Serve</h2>
                </div>
                <div class="clients-logo row">
                    <ul>
                        <li>
                            <!-- <a href="javascript:void(0)"> -->
                                <figure>
                                    <img src="{{asset(''.section($page,'Section 4 Company 1 Image').'')}}" alt="Client">
                                </figure>
                            <!-- </a> -->
                        </li>
                        <li>
                            <!-- <a href="javascript:void(0)"> -->
                                <figure>
                                    <img src="{{asset(''.section($page,'Section 4 Company 2 Image').'')}}" alt="Client">
                                </figure>
                            <!-- </a> -->
                        </li>
                        <li>
                            <!-- <a href="javascript:void(0)"> -->
                                <figure>
                                    <img src="{{asset(''.section($page,'Section 4 Company 3 Image').'')}}" alt="Client">
                                </figure>
                            <!-- </a> -->
                        </li>
                        <li>
                            <!-- <a href="javascript:void(0)"> -->
                                <figure>
                                    <img src="{{asset(''.section($page,'Section 4 Company 4 Image').'')}}" alt="Client">
                                </figure>
                            <!-- </a> -->
                        </li><li>
                            <!-- <a href="javascript:void(0)"> -->
                                <figure>
                                    <img src="{{asset(''.section($page,'Section 4 Company 5 Image').'')}}" alt="Client">
                                </figure>
                            <!-- </a> -->
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        {{-- Safety First --}}
        <section class="section--safety-first-section">
            <div class="wrapper">
                <div class="section section--safety-first-section__content bg"  style="background-image: url('{{settings('SS0008')}}')">
                    <div class="row">
                        <div class="col-lg-6 col-sm-7 offset-sm-2 offset-lg-6">
                            <div class="d-flex">
                                {!!settings('SS0009')!!}
                                {{-- <h2>safety first.</h2>
                                <a href="{{url('about-us#employee-safety')}}" class="btn btn--primary">Our Services</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('front.layouts.sections.footer')
</section>
