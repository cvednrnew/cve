<section class="page page--team">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            {{-- <img src="{{asset('public/img/background/team-banner_bg.jpg')}}" alt="Team"> --}}
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Team">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                    {{-- <h1>Our Team</h1> --}}
                </div>
            </div>
        </section>
        {{-- END OF BANNER --}}
        <div class="bg">

            {{-- Services Detail Content --}}
            <section class="section--team-overview">
                <div class="wrapper">
                    
                    <div class="section--team-overview--content ">
                        {{-- <strong class="heading">Experienced Staff</strong> --}}
                        <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                        <div class="row">
                            <div class="col-lg-6">
                                {!!section($page,'Section 1 Content')!!}
                                {{-- <h2>power for companies, powered by people.</h2>
                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit dolor sup</h4>
                                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                                <a href="{{url('contact-us')}}" class="btn btn--primary">Join Our Team!</a> --}}
                            </div>
                            <div class="col-lg-6">
                                <div class="row team-list featured-team">

                                    @foreach (\App\Models\Leadership::limit(2)->get() as $index => $team_two_row)
                                        
                                    <div class="col-lg-6 col-sm-6 team--item">
                                        <a href="{{url('team-details/'.$team_two_row->slug)}}" class="team-member-box {{$index == 0 ? '' : 'top-spacing'}}">
                                            <img src="{{asset(''.$team_two_row->banner_image.'')}}" alt="{{$team_two_row->name}}">
                                            <div class="overlay">
                                                <h3>{{$team_two_row->name}}</h3>
                                                <p>{{$team_two_row->position}}</p>
                                            </div>
                                        </a>
                                    </div>

                                    @endforeach

                                    {{-- <div class="col-lg-6">
                                        <a href="{{url('team-details')}}" class="team-member-box">
                                            <img src="{{asset('public/img/team/team-member-1.jpg')}}" alt="Team Member 1">
                                            <div class="overlay">
                                                <h3>Lance Neal</h3>
                                                <p>Vice President of Operations</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="{{url('team-details')}}" class="team-member-box top-spacing">
                                            <img src="{{asset('public/img/team/team-member-2.jpg')}}" alt="Team Member 2">
                                            <div class="overlay">
                                                <h3>Lance Neal</h3>
                                                <p>Vice President of Operations</p>
                                            </div>
                                        </a>
                                    </div> --}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{-- End of Services Content --}}

            {{-- Services Detail function --}}
            <section class="section--team-member">
                <div class="wrapper">
                    @php
                        $counter = 1;
                    @endphp

                    @foreach (\App\Models\Leadership::where('is_active',1)->get() as $index => $team)

                        @if($index >= 2)

                            @if($counter == 1)
                                <div class="row team-list">
                            @endif

                                <div class="col-lg-3 col-sm-6 team--item">
                                    <a href="{{url('team-details/'.$team->slug)}}" class="team-member-box {{$index % 2 == 0 ? '' : 'top-spacing'}}">
                                        <img src="{{asset(''.$team->banner_image.'')}}" alt="{{$team->name}}">
                                        <div class="overlay">
                                            <h3>{{$team->name}}</h3>
                                            <p>{{$team->position}}</p>
                                        </div>
                                    </a>
                                </div>

                            @if($counter == 4)
                                </div>
                                
                                @php 
                                    $counter = 0;
                                @endphp
                                
                            @endif

                            @php 
                                $counter++;
                            @endphp
                        @endif

                    @endforeach


                    {{-- <div class="row">

                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-3.jpg')}}" alt="Team Member 3">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-4.jpg')}}" alt="Team Member 4">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-5.jpg')}}" alt="Team Member 5">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-6.jpg')}}" alt="Team Member 6">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-7.jpg')}}" alt="Team Member 7">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-4.jpg')}}" alt="Team Member 4">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box">
                                <img src="{{asset('public/img/team/team-member-5.jpg')}}" alt="Team Member 5">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('team-details')}}" class="team-member-box top-spacing">
                                <img src="{{asset('public/img/team/team-member-6.jpg')}}" alt="Team Member 6">
                                <div class="overlay">
                                    <h3>Lance Neal</h3>
                                    <p>Vice President of Operations</p>
                                </div>
                            </a>
                        </div>

                    </div> --}}
                </div>
            </section>

            {{-- End of Services detail function --}}

        </div>

        
    </main>
    @include('front.layouts.sections.footer')
</section>