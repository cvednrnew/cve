@extends('front.layouts.base')

@section('content')

<section class="page page--services-detail">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">

        <div class="bg">
        
            {{-- Services Detail Banner --}}
            <section class="section--services-detail-banner">
                <div class="wrapper">
                    <a href="{{url('services')}}" class="btn btn--back">Back to Services</a>
                    <div class="banner-img">
                        <img src="{{asset(''.$service->banner_image.'')}}" alt="Services Detail image">
                        {{-- <img src="{{asset('public/img/services/service-detail-banner.jpg')}}" alt="Services Detail image"> --}}
                    </div>
                </div>
            </section>
            {{-- End of Services Detail Banner --}}

            {{-- Services Detail Content --}}
            <section class="section--services-overview">
                <div class="wrapper">
                    
                    <div class="section--services-overview--content ">
                        <strong class="heading">Our Services</strong>

                        {!!$service->content!!}

                        {{-- <h2>industrial solutions for Complex Challenges</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>The challenges of ensuring electrical power systems at industrial operations like mining, utilities and power generation are so great that many electrical contractors don’t play in this space. At CVE, however, this is</p>
                            </div>
                            <div class="col-lg-6">
                                <p>one of our specialty areas; we have extensive experience designing, install-ing, maintaining, repairing and managing the complex electrical power systems that industries rely on to sustain their operations.</p>
                            </div>
                        </div> --}}
                        
                    </div>
                </div>
            </section>
            {{-- End of Services Content --}}

            {{-- Services Detail function --}}
            <section class="section--services-detail-function">
                <div class="wrapper">
                    <div class="content">
                        <div class="row">

                            <div class="col-md-4">
                                {{-- <img src="{{asset('public/img/services/service-detail-function.jpg')}}" alt="Services Detail function image"> --}}
                                <img src="{{asset(''.$service->file.'')}}" alt="Services Detail function image">
                            </div>
                            
                            <div class="col-md-8">
                                
                                {!!$service->content_1!!}

                                {{-- <h2>Function at a High Level</h2>
                                <h3>Your Electrical System Will Benefit from our Industrial Experience</h3>
                                <p>Our   experienced team    has   a  well-deserved reputation for   excellence,so  we’re  excited  about  demonstrating  our  abilities  in  the  industrial  sector. We   understand your    need    for   efficient, cost-effective electricalsystems that    offer    24/7    uptime to  support your    operation. </p>
                                <ul>
                                    <li>
                                        <p>Generators</p>
                                    </li>
                                    <li>
                                        <p>Industrial Plant Maintenance</p>
                                    </li>
                                    <li>
                                        <p>Hydro-pumping Stations</p>
                                    </li>
                                </ul>
                                <a href="{{url('contact-us')}}" class="btn btn--primary">Contact Us</a>
                                <a href="{{url('services')}}" class="btn btn--white">Other Services</a> --}}
                                <a href="{{url('contact-us')}}" class="btn btn--primary">Contact Us</a>
                                <a href="{{url('services')}}" class="btn btn--white">Other Services</a> 
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>

            {{-- End of Services detail function --}}

        </div>

        {{-- PROJECTS SECTION --}}
        <section class="section--services-projects-related">
            <div class="project--lists">
                <div class="heading row clearfix">
                    <div class="col-lg-12">
                        <h2>Related Projects</h2>
                        <ul>
                            <li>
                                <a href="javascript:void(0)" class="project-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="project-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <style> 

                    .related-project--slider .slick-track {
                       margin-left: initial;
                    }
                    
                </style>
                <ul class="related-project--slider">
             
                    @php
                    $previous =\App\Models\Service::where('id', '<', $service->id)->where('is_active',1)->orderBy('id','desc')->get();
                    $next = \App\Models\Service::where('id', '>', $service->id)->where('is_active',1)->orderBy('id','asc')->get();
                    $related_service = \App\Models\Project::where('category_service_arr', 'like', '%'.$service->id.'%')->where('is_active',1)->orderBy('order','asc')->get()
                    @endphp

                    @foreach ($related_service as $item)
                    @if($service->id != $item->id)
                        <li>
                            <div class="relative">
                                <figure>
                                    <img src="{{asset(''.$item->banner_image.'')}}" alt="{{$item->name}}">
                                </figure>
                                <div class="caption">
                                    <h3><a href="{{url('project-details/'.$item->slug.'')}}">{{$item->name}}</a></h3>
                                </div>
                            </div>
                        </li>
                    @endif
                    @endforeach

                    {{-- next rec --}}
                    {{-- @foreach ($next as $item)
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset(''.$item->banner_image.'')}}" alt="{{$item->name}}">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('service-details/'.$item->slug.'')}}">{{$item->name}}</a></h3>
                            </div>
                        </div>
                    </li>
                    @endforeach  --}}

                    {{-- prev rec --}}
                    {{-- @foreach ($previous as $item)
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset(''.$item->banner_image.'')}}" alt="{{$item->name}}">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('service-details/'.$item->slug.'')}}">{{$item->name}}</a></h3>
                            </div>
                        </div>
                    </li>
                    @endforeach --}}

                    {{-- <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/services/related-service-1.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}">Chula Vista Electric System</a></h3>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/services/related-service-2.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}">Panasonic Factory</a></h3>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/project-3.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}"> High voltage Systemvoltate?</a></h3>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/project-1.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                            </div>
                        </div>
                    </li> --}}
                </ul>
            </div>
        </section>

        {{-- Service Detail Pagination --}}
        <section class="section--services-detail-pagination">
            <div class="wrapper">
                <div class="content">
                    <div class="column-left">
                        @if(count($previous))
                        <a href="{{url('service-details/'.$previous[0]->slug.'')}}" class="btn btn--back">{{$previous[0]->name}}</a>
                        <a href="{{url('service-details/'.$previous[0]->slug.'')}}" class="btn btn--gray">Previous</a>
                        @endif
                    </div>
                    <div class="column-right">
                        @if(count($next))
                        <a href="{{url('service-details/'.$next[0]->slug.'')}}" class="btn btn--next">{{$next[0]->name}}</a>
                        <a href="{{url('service-details/'.$next[0]->slug.'')}}" class="btn btn--gray">Next</a>
                        @endif
                    </div>
                    {{-- <div class="column-left">
                        <a href="#" class="btn btn--back">Design Build</a>
                        <a href="#" class="btn btn--gray">Previous</a>
                    </div>
                    <div class="column-right">
                        <a href="#" class="btn btn--next">Network System</a>
                        <a href="#" class="btn btn--gray">Next</a>
                    </div> --}}
                </div>
            </div>
        </div>
        {{-- End of Service Detail Pagination --}}
        

        
    </main>
    @include('front.layouts.sections.footer')
</section>

@endsection