<section class="hero">
    <div class="hero-slider text-center">

        @foreach (\App\Models\HomeSlide::all() as $slides)
            <div class="bg slider-image" style="background-image: url('{{asset(''.$slides->background_image.'')}}');">
               <img src="{{asset(''.$slides->background_image.'')}}" alt="" class="d-none">
                <div class="flex">
                    <div class="caption">
                        {{-- <h1>Power Your Business With the Power of CVE</h1> --}}
                        {!!$slides->content!!}
                        <a href="{{$slides->button_link}}" class="btn btn--primary mr-3">{{$slides->button_label}}</a>
                        <a href="{{$slides->button_link_1}}" class="btn btn--primary btn--white">{{$slides->button_label_1}}</a>
                    </div>
                </div>
            </div>

        @endforeach

        {{-- <div class="bg" style="background-image: url('{{asset('public/img/hero.jpg')}}');">
            <div class="flex">
                <div class="caption">
                    <h1>Power Your Business With the Power of CVE123</h1>
                    <a href="#" class="btn btn--primary mr-3">Services</a>
                    <a href="#" class="btn btn--primary btn--white">Contact Us</a>
                </div>
            </div>
        </div> --}}

    </div>


    <div class="hero-slide-navigation">
        <div>
            <a href="#" class="arrow hero-prev">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                <div class="arrow-prev_img">
                </div>
            </a>
            
            <a href="#" class="arrow hero-next">
                <div class="arrow-next_img">
                </div>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
        </div>

    </div>
</section>