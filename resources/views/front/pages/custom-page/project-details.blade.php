@extends('front.layouts.base')

@section('content')

<section class="page page--services-detail page--project-detail ">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">

        <div class="bg">
            {{-- Services Detail Banner --}}
            <section class="section--services-detail-banner">
                <div class="wrapper">
                    <a href="{{url('projects')}}" class="btn btn--back">Back to Projects</a>
                    <div class="banner-img">
                        {{-- <img src="{{asset('public/img/project-1.jpg')}}" alt="Project Detail image"> --}}
                        <img src="{{asset(''.$project->banner_image.'')}}" alt="Project Detail image">
                    </div>
                </div>
            </section>
            {{-- End of Services Detail Banner --}}

            {{-- Services Detail Content --}}
            <section class="section--services-overview">
                <div class="wrapper">
                    
                    <div class="section--services-overview--content ">
                        <strong class="heading">Our Projects</strong>

                        {!!$project->content!!}
                        
                        {{-- <h2>industrial solutions for Complex Challenges</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>The challenges of ensuring electrical power systems at industrial operations like mining, utilities and power generation are so great that many electrical contractors don’t play in this space. At CVE, however, this is</p>
                            </div>
                            <div class="col-lg-6">
                                <p>one of our specialty areas; we have extensive experience designing, install-ing, maintaining, repairing and managing the complex electrical power systems that industries rely on to sustain their operations.</p>
                            </div>
                        </div> --}}
                        
                    </div>
                    <div class="gallery--heading">
                        <h3>Project Gallery</h3>
                    </div>
                    <div class="gallery--container">
                        <div class="gallery">
                            @if(!empty($project->gallery))
                                @foreach ($project->gallery as $index => $item)
                                <div class="column">
                                    <div class="item">
                                        <a href="{{asset(''.$item->image.'')}}" class="image-modal">
                                            <img src="{{asset(''.$item->image.'')}}" class="img" >
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            @endif
    
                            {{-- @php
                            $count = 0;
                            @endphp
    
                            @foreach ($project->gallery as $index => $item)
    
                            @php
                            $count++;
                            @endphp
    
                            @if($count == 1)
                            <div class="column">
                            @endif
    
                            <div class="item">
                                <a href="{{asset(''.$item->image.'')}}" class="image-modal">
                                    <img src="{{asset(''.$item->image.'')}}" class="img">
                                </a>
                            </div>
                                
                            @if($count == 3)
                                </div>
                                @php
                                $count = 0;
                                @endphp
                            @elseif(count($project->gallery) == $index)
                                </div>
                            @endif
    
                            @endforeach --}}
    
                            {{-- <div class="column">
                                <div class="item">
                                    <a href="{{asset('public/img/project-1.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/project-1.jpg')}}" class="img">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('public/img/project-2.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/project-2.jpg')}}" class="img">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('public/img/sample-img_1.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/sample-img_1.jpg')}}" class="img">
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="item">
                                    <a href="{{asset('public/img/services/service-detail-function.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/services/service-detail-function.jpg')}}" class="img">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('public/img/services/service-detail-banner.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/services/service-detail-banner.jpg')}}" class="img">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('public/img/services/related-service-1.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/services/related-service-1.jpg')}}" class="img">
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="item">
                                    <a href="{{asset('public/img/sample-img_1.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/sample-img_1.jpg')}}" class="img">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('public/img/services/service-detail-banner.jpg')}}" class="image-modal">
                                        <img src="{{asset('public/img/services/service-detail-banner.jpg')}}" class="img">
                                    </a>
                                </div>
                            </div> --}}
    
                        </div>
                    </div>
                    
                </div>
            </section>
            {{-- End of Services Content --}}

            {{-- CTA Form --}}
            {{-- <section class="section--project-cta-form">
                <div class="wrapper">
                    <div class="section--project-cta-form--content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text">
                                    <strong class="heading">Services</strong>
                                    <h2>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aspernatur, molestias rem nisi a quasi, laborum perferendis eligendi necessitatibus magnam et voluptate alias neque provident natus quis. Iusto suscipit nobis molestiae.</p>
                                    <a href="{{url('services')}}" class="btn btn--primary">View All Services</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form">
                                    {{  Form::open([
                                        'method' => 'POST',
                                        'id' => 'create-contact',
                                        'route' => ['contact.store'],
                                        'class' => '',
                                        ])
                                    }}
                                    <ul class="form-box row">
                                        <div class="col-md-6 form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                            <input id="firstname" type="text" class="form-control " name="firstname" required>
                                            <label for="firstname">First Name</label>
                                            @if($errors->has('firstname'))
                                                <span class="help-block animation-slideDown">{{ $errors->first('firstname') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                            <input id="lastname" type="text" class="form-control " name="lastname" required>
                                            <label for="lastname">Last Name</label>
                                            @if($errors->has('lastname'))
                                                <span class="help-block animation-slideDown">{{ $errors->first('lastname') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <input id="phone" type="text" class="form-control " name="phone" required>
                                            <label for="phone">Phone</label>
                                            @if($errors->has('phone'))
                                                <span class="help-block animation-slideDown">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email" type="text" class="form-control " name="email" required>
                                            <label for="email">Email</label>
                                            @if($errors->has('email'))
                                                <span class="help-block animation-slideDown">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-12 form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                            <textarea id="message" name="message" class="form-control" required></textarea>
                                            <label for="message">Message</label>
                                            @if($errors->has('message'))
                                                <span class="help-block animation-slideDown">{{ $errors->first('message') }}</span>
                                            @endif
                                        </div>
                                        <div
                                                class="col-md-12 form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                            {!! NoCaptcha::display() !!}
                                            @if($errors->has('g-recaptcha-response'))
                                                <span
                                                        class="help-block animation-slideDown">{{ $errors->first('g-recaptcha-response') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <button type="submit" class="btn btn--primary" value="Submit">Submit</button>
                                        </div>
                                    </ul>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            {{-- Services Detail function --}}
            {{-- <section class="section--services-detail-function">
                <div class="wrapper">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="{{asset('public/img/services/service-detail-function.jpg')}}" alt="Services Detail function image">
                            </div>
                            <div class="col-lg-8">
                                <h2>Function at a High Level</h2>
                                <h3>Your Electrical System Will Benefit from our Industrial Experience</h3>
                                <p>Our   experienced team    has   a  well-deserved reputation for   excellence,so  we’re  excited  about  demonstrating  our  abilities  in  the  industrial  sector. We   understand your    need    for   efficient, cost-effective electricalsystems that    offer    24/7    uptime to  support your    operation. </p>
                                <ul>
                                    <li>
                                        <p>Generators</p>
                                    </li>
                                    <li>
                                        <p>Industrial Plant Maintenance</p>
                                    </li>
                                    <li>
                                        <p>Hydro-pumping Stations</p>
                                    </li>
                                </ul>
                                <a href="{{url('contact-us')}}" class="btn btn--primary">Contact Us</a>
                                <a href="{{url('projects')}}" class="btn btn--white">Other Projects</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            {{-- End of Services detail function --}}

        </div>

        {{-- PROJECTS SECTION --}}
        <section class="section--services-projects-related">
            <div class="project--lists">
                <div class="heading row clearfix">
                    <div class="col-lg-12">
                        <h2>Other Projects</h2>
                        <ul>
                            <li>
                                <a href="javascript:void(0)" class="project-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="project-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul class="related-project--slider">
                    @php
                    $previous =\App\Models\Project::where('id', '<', $project->id)->where('is_active',1)->orderBy('id','desc')->get();
                    $next = \App\Models\Project::where('id', '>', $project->id)->where('is_active',1)->orderBy('id','asc')->get();
                    @endphp

                    {{-- next rec --}}
                    @foreach ($next as $item)
                        <li>
                            <a href="{{url('project-details/'.$item->slug.'')}}"class="relative">
                                <figure>
                                    <img src="{{asset(''.$item->banner_image.'')}}" alt="{{$item->name}}">
                                </figure>
                                <div class="caption">
                                    <h3>{{$item->name}}</h3>
                                </div>
                            </a>
                        </li>
                    @endforeach

                    {{-- prev rec --}}
                    @foreach ($previous as $item)
                        <li>
                            <div class="relative">
                                <figure>
                                    <img src="{{asset(''.$item->banner_image.'')}}" alt="{{$item->name}}">
                                </figure>
                                <div class="caption">
                                    <h3><a href="{{url('project-details/'.$item->slug.'')}}">{{$item->name}}</a></h3>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>

        {{-- Service Detail Pagination --}}

        <section class="section--services-detail-pagination">
            <div class="wrapper">
                <div class="content">
                    <div class="column-left">
                        @if(count($previous))
                        <a href="{{url('project-details/'.$previous[0]->slug.'')}}" class="btn btn--back">{{$previous[0]->name}}</a>
                        <a href="{{url('project-details/'.$previous[0]->slug.'')}}" class="btn btn--gray">Previous</a>
                        @endif
                    </div>
                    <div class="column-right">
                        @if(count($next))
                        <a href="{{url('project-details/'.$next[0]->slug.'')}}" class="btn btn--next">{{$next[0]->name}}</a>
                        <a href="{{url('project-details/'.$next[0]->slug.'')}}" class="btn btn--gray">Next</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {{-- End of Service Detail Pagination --}}
        

        
    </main>
    @include('front.layouts.sections.footer')
</section>

@endsection

