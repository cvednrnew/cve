<section class="page page--contact-us">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">

        {{-- BANNER --}}
        <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Contact Us">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
        {{-- END OF BANNER --}}

        {{-- 3 LINKS --}}
        <section class="section--links">
            <div class="wrapper_1500">
                <div class="section--links__content">
                    <div class="row">

                        <div class="col-lg-3 section--links__content-item phone">
                            <h3>Call Us 24/7 <span class="line"></span></h3>
                            <img src="{{asset('public/img/phone_icon_colored.png')}}" alt="Phone Icon">
                            <div class="text">
                                <span>Phone</span>
                                <p><a href="tel:{{settings('SS0003')}}">{{settings('SS0003')}}</a></p>
                            </div>
                        </div>
                        <div class="col-lg-3 section--links__content-item email">
                            <h3>Email Us <span class="line"></span></h3>
                            <img src="{{asset('public/img/email_icon-colored.png')}}" alt="Email Icon">
                            <div class="text">
                                <span>Email</span>
                                <p><a href="mailto:{{settings('SS0002')}}">{{settings('SS0002')}}</a></p>
                            </div>
                        </div>
                        <div class="col-lg-6 section--links__content-item address">
                            <h3>Address <span class="line"></span></h3>
                            <img src="{{asset('public/img/pin_icon-colored.png')}}" alt="Address Icon">
                            <div class="text">
                                <span>san Diego Office</span>
                                <p>{{settings('SS0004')}}</p>
                            </div>
                            <div class="text">
                                <span>Imperial VAlley Office</span>
                                <p>{{settings('SS0012')}}</p>
                            </div>
                        </div>

                    </div> 
                </div>
            </div>
        </section>
        {{-- END OF 3 LINKS --}}

        {{-- Form and Google MAp Section --}}
        <section class="section--contact-info">
            <div class="section--contact-info__content clearfix">
                <div class="floatLeft">
                     <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                    <p>
                        {{section($page,'Section 1 Description')}}
                    </p>
                    <div class="contact-form">
                        <h2>{{section($page,'Section 2 Form Title')}}</h2>
                        {{  Form::open([
                            'method' => 'POST',
                            'id' => 'create-contact',
                            'route' => ['contact.store'],
                            'class' => '',
                            ])
                        }}
                        
                        <ul class="form-box row">
                            <div class="col-md-12 form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <input id="firstname" type="text" class="form-control " name="firstname" required>
                                <label for="firstname">First Name</label>
                                @if($errors->has('firstname'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('firstname') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <input id="lastname" type="text" class="form-control " name="lastname" required>
                                <label for="lastname">Last Name</label>
                                @if($errors->has('lastname'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('lastname') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="text" class="form-control " name="email" required>
                                <label for="email">Email</label>
                                @if($errors->has('email'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea id="message" name="message" class="form-control" required></textarea>
                                <label for="message">Message</label>
                                @if($errors->has('message'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('message') }}</span>
                                @endif
                            </div>
                            {{-- <div
                                    class="col-md-12 form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                {!! NoCaptcha::display() !!}
                                @if($errors->has('g-recaptcha-response'))
                                    <span
                                            class="help-block animation-slideDown">{{ $errors->first('g-recaptcha-response') }}</span>
                                @endif
                            </div> --}}
                            <div class="col-md-12 form-group">
                                <button type="submit" class="btn btn--primary" value="Submit">Submit</button>
                            </div>
                        </ul>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="floatRight">
                    {!!section($page,'Section iFrame')!!}
                    {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3351.9897984363447!2d-116.96360678449186!3d32.845521780951465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dbe29ff4712b0b%3A0xbc28748b99c7571e!2s9344%20Wheatlands%20Rd%2C%20Santee%2C%20CA%2092071%2C%20USA!5e0!3m2!1sen!2sph!4v1614666637143!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy">
                    </iframe> --}}

                </div>
            </div>
        </section>

        {{-- End of Form and Google MAp Section --}}

        {{-- individual section  --}}
        <section class="section-name">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 section-name__item">
                        
                    </div>
                </div>
            </div> {{-- end of default-content--row --}}
        </section>
    </main>
    @include('front.layouts.sections.footer')
</section>
@push('extrascripts')
{!! NoCaptcha::renderJs() !!}
<script type="text/javascript" src="{{ asset('public/js/libraries/contacts.js') }}"></script>
@endpush