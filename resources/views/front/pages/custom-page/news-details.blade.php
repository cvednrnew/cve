@extends('front.layouts.base')

@section('content')

<section class="page page--news-details">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        
        <section class="section--news-details bg" style="background-image:url('{{asset('public/img/background/news-blogs_bg.jpg')}}');">
            <div class="wrapper clearfix">
                <div class="section--news-details--content floatLeft">
                    <strong class="heading">News & Events</strong>
                    <a href="{{url('news')}}" class="btn btn--back">Back to News</a>
                    <div class="heading">

                        <h1>{{$news->name}}</h1>
                        <span class="category">{{$news->news_category->name}}</span>
                        <span class="date">{{  Carbon\Carbon::parse($news->date)->format('F d, Y') }}</span>
                        @if($news->link != '')
                        <span class="website-link"><a href="{{add_http($news->link)}}"> Website Link ></a></span>
                        @endif


                        {{-- <h1>This is a title of news</h1>
                        <span class="category">Union Tribune</span>
                        <span class="date">October 6, 2020</span>
                        <span class="website-link"><a href="#"> Website Link ></a></span> --}}
                    </div>
                    <div class="text">
                        <article>

                            <figure>
                                <img src="{{asset(''.$news->banner_image.'')}}" alt="News Details">
                            </figure>

                            {!!$news->content_1!!}
                            
                            {!!$news->content_2!!}

                            {{-- <figure>
                                <img src="{{asset('public/img/news/news-details_img.jpg')}}" alt="News Details">
                            </figure>

                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                           
                            <h2>FIrst Headline</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        --}}
                        </article>
                    </div>
                </div>

                @php
                $other_news = \App\Models\News::orderBy('date','desc')->limit(6)->get();
                @endphp

                <sidebar class="section--news-details--sidebar floatRight">
                    <div class="other-news">
                        <h3>Other News</h3>
                        <ul>

                            @foreach ($other_news as $item)
                            @if($news->id != $item->id)
                            <li>
                                <a href="{{url('news-details/'.$item->slug.'')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset(''.$item->banner_image.'')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>{{$item->name}}</h2>
                                    </div>
                                </a>
                            </li>
                            @endif
                            @endforeach

                            {{-- <li>
                                <a href="{{url('team-details')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset('public/img/news/other-news-1.jpg')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>Altus Power acquires five solar farms from CVE</h2>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('team-details')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset('public/img/news/other-news-2.jpg')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicingvoluptate velit</h2>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('team-details')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset('public/img/news/other-news-3.jpg')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>Sed do eiusmod tempor incididunt ut Aabore et dolore magna aliqua. Ut </h2>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('team-details')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset('public/img/news/other-news-4.jpg')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>Mska aliqua. Ut enim ad minim veniam</h2>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('team-details')}}">
                                    <div class="relative img-outer">
                                        <img src="{{asset('public/img/news/other-news-5.jpg')}}" alt="News Details">
                                    </div>
                                    <div class="title">
                                        <h2>Altus Power acquires five solar farms from CVE</h2>
                                    </div>
                                </a>
                            </li> --}}
                            

                        </ul>
                    </div>
                </sidebar>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>

@endsection
