<section class="page page--renewable-energy">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        
         {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Renewable Energy">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
         {{-- END OF BANNER --}}

        {{-- Services Content --}}
        <section class="section--renewable-energy-overview">
            <div class="wrapper">
                <div class="section--renewable-energy-overview--content">

                    <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>

                    {!!section($page,'Section 1 Content')!!}
                    {{-- <h2>The Power of the Future — Today. </h2>
                    <h3>Ready to go Green?</h3>
                    <p>We’ve seen a lot of change since 1925, when CVE was founded. Throughout the years, we’ve evolved  to  meet  new  demands,  and  today  we  stand  ready  to  help  you  enhance  your  green  footprint in a variety of ways, including solar, wind generation, geothermal, hydroelectric, micro grid, fuel cells, and battery storage. We even have the expertise to install and maintain electric vehicle charge stations. Step into the future today with our help.</p>
                    --}}
              
                     <div class="row">
                    {!!section($page,'Section 2 Content')!!}

                        {{--<div class="col-lg-4">
                            <div class="list-con">
                                <div class="list-con--icon">
                                    <img src="{{asset('public/img/renewable-energy/renewable-energy-icon-1.png')}}" alt="Renewable Energy">
                                    <div class="title">Consult/Plan</div>
                                </div>
                                <ul>
                                    <li>Learn Specific Needs</li>
                                    <li>Define Expectations</li>
                                    <li>Design Your System</li>
                                    <li>Establish Goals</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="list-con">
                                <div class="list-con--icon">
                                    <img src="{{asset('public/img/renewable-energy/renewable-energy-icon-2.png')}}" alt="Renewable Energy">
                                    <div class="title">Install/Maintain</div>
                                </div>
                                <ul>
                                    <li>Plan For Safety</li>
                                    <li>Use Creativity</li>
                                    <li>Provide ROI</li>
                                    <li>Focus on Quantity</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="list-con">
                                <div class="list-con--icon">
                                    <img src="{{asset('public/img/renewable-energy/renewable-energy-icon-3.png')}}" alt="Renewable Energy">
                                    <div class="title">Repair/Replace</div>
                                </div>
                                <ul>
                                    <li>Maintain Oerations</li>
                                    <li>Respond Quickly</li>
                                    <li>Fix Critical Systems</li>
                                    <li>Enhance Functionality</li>
                                </ul>
                            </div>
                        </div>--}}
                    </div> 

                    
                </div>
            </div>
        </section>
        {{-- End of Services Content --}}
        
        <section class="section--innovation">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="overview">
                            <strong class="heading">{{section($page,'Section 3 Left Title')}}</strong>
                            {!!section($page,'Section 3 Col 1 Content')!!}
                            
                           {{-- <h3>Innovative Technologies</h3>
                            <h4>Discover the Benefits & Challenges of Renewable Energy</h4>
                            <p>The area of renewable energy is quickly evolving, and we’re keeping up with the changes to ensure we can provide the   best service to you. Whether you know which clean energy alternative you prefer, or you need to learn more before deciding, we can be of help when it comes time to design and install your system, as well as maintain it.</p>
                            <a href="{{url('contact-us')}}" class="btn btn--primary">Contact Us Today!</a> --}}
                        
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <a href="{{url(''.section($page,'Section 3 Col 2 Link 1') == '' ? '#' : section($page,'Section 3 Col 2 Link 1').'')}}" class="section--innovation--item bg" style="pointer-events:none;background-image: url('{{asset(''.section($page,'Section 3 Col 2 Image 1').'')}}')">
                                    <div class="title">
                                        <h3>{{section($page,'Section 3 Col 2 Text 1')}}</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <a href="{{url(''.section($page,'Section 3 Col 2 Link 2') == '' ? '#' :section($page,'Section 3 Col 2 Link 2').'')}}" class="section--innovation--item bg" style="pointer-events:none;background-image: url('{{asset(''.section($page,'Section 3 Col 2 Image 2').'')}}')" >
                                    <div class="title">
                                        <h3>{{section($page,'Section 3 Col 2 Text 2')}}</h3>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12">
                                <a href="{{url(''.section($page,'Section 3 Col 2 Link 3') == '' ? '#' :section($page,'Section 3 Col 2 Link 3').'')}}" class="section--innovation--item bg" style="pointer-events:none;background-image: url('{{asset(''.section($page,'Section 3 Col 2 Image 3').'')}}')" >
                                    <div class="title">
                                        <h3>{{section($page,'Section 3 Col 2 Text 3')}}</h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

        {{-- CLIENTS SECTION --}}
        <section class="section--clients" style="background-image: url({{asset('public/img/service-detail-bg.jpg')}})">
            <div class="wrapper">
                <div class="row">
                    {!!section($page,'Section 4 Content')!!}
                    {{-- <div class="col-lg-6">
                        <div class="overview">
                            <strong class="heading">Experienced Team</strong>
                            
                            <h3>Experience that Matters</h3>
                            <h4>Let’s Step into The Future Together</h4>
                            <p>Our experienced team has a well-deserved reputation for   excellence, so we’re excited about demonstrating our abilities in the exciting and evolving area of renewable energy. After we learn your specific needs, we’ll design/install a state-of-the-art system that will meet or exceed them.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="overview">
                            <p><em>“CVE did a great job on our project it was a very professional installation. The project was well planned and executed through all phases. As always, it was a pleasure working with CVE.”</em></p>
                            <p class="alt"><strong>John Williams</strong></p>
                            <p class="alt"><strong>CHULA VISTA MALL</strong></p>
                        </div>
                    </div> --}}
                </div>
                <div class="clients-con">
                    <div class="title">{{section($page,'Section 5 Heading')}}</div>
                    <div class="clients-icon">
                        <div class="item">
                            <img src="{{asset(''.section($page,'Section 5 Image 1').'')}}" alt="Renewable Energy">
                        </div>
                        <div class="item">
                            <img src="{{asset(''.section($page,'Section 5 Image 2').'')}}" alt="Renewable Energy">
                        </div>
                        <div class="item">
                            <img src="{{asset(''.section($page,'Section 5 Image 3').'')}}" alt="Renewable Energy">
                        </div>
                        <div class="item">
                            <img src="{{asset(''.section($page,'Section 5 Image 4').'')}}" alt="Renewable Energy">
                        </div>
                        <div class="item">
                            <img src="{{asset(''.section($page,'Section 5 Image 5').'')}}" alt="Renewable Energy">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- PROJECTS SECTION --}}
        <section class="section--projects">
            <div class="project--lists">
                <div class="heading row clearfix">
                    <div class="col-lg-12">
                        <h2>Related Projects</h2>
                        <ul>
                            <li>
                                <a href="javascript:void(0)" class="project-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="project-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul class="project--slider">

                    @foreach (\App\Models\Project::where('is_active',1)->get() as $item)
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset(''.$item->banner_image.'')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details/'.$item->slug.'')}}"> {{$item->name}}</a></h3>
                                <a href="{{url('project-details/'.$item->slug.'')}}" class="btn btn--learnmore">Learn More</a>
                            </div>
                        </div>
                    </li>
                    @endforeach

                    {{-- <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/project-1.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/project-2.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="relative">
                            <figure>
                                <img src="{{asset('public/img/project-3.jpg')}}" alt="Project">
                            </figure>
                            <div class="caption">
                                <h3><a href="{{url('project-details')}}"> Mercy West Hospital</a></h3>
                                <a href="{{url('project-details')}}" class="btn btn--learnmore">Learn More</a>
                            </div>
                        </div>
                    </li> --}}


                </ul>
            </div>
        </section>
    </main>
    @include('front.layouts.sections.footer')
</section>