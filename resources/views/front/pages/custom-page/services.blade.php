<section class="page page--services">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        
         {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="Services">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
         {{-- END OF BANNER --}}

        {{-- Services Content --}}
        <section class="section--services-overview">
            <div class="wrapper">
                <div class="section--services-overview--content">
                    <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                    <h2>{{section($page,'Section 1 Text')}}</h2>
                    <div class="row">
                        <div class="col-lg-6">
                            {!!section($page,'Section 1 Col 1 Content')!!}
                        </div>
                        <div class="col-lg-6">
                            {!!section($page,'Section 1 Col 2 Content')!!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- End of Services Content --}}
        
        <section class="section--list-of-services">
            <div class="row">
                @foreach (\App\Models\Service::where('is_active',1)->get() as $item)

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details/'.$item->slug)}}" class="section--list-of-services--item bg" style="background-image: url('{{asset(''.$item->banner_image.'')}}')">
                        <div class="title">
                            <h3>{{$item->name}}</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>

                @endforeach
                {{-- <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-1.jpg')}}')">
                        <div class="title">
                            <h3>Commercial</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-2.jpg')}}')">
                        <div class="title">
                            <h3>Design Build</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-3.jpg')}}')">
                        <div class="title">
                            <h3>Healthcare</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-4.jpg')}}')">
                        <div class="title">
                            <h3>Industrial</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-5.jpg')}}')">
                        <div class="title">
                            <h3>Network Systems</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-6.jpg')}}')">
                        <div class="title">
                            <h3>Medium & High Voltage</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-7.jpg')}}')">
                        <div class="title">
                            <h3>Renewable Energy</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <a href="{{url('service-details')}}" class="section--list-of-services--item bg" style="background-image: url('{{asset('public/img/services/service-8.jpg')}}')">
                        <div class="title">
                            <h3>Preventative Maintenance</h3>
                        </div>
                        <span class="view-details">View Details</span>
                    </a>
                </div> --}}
               
                <div class="col-lg-4 col-md-12">
                    <div class="section--list-of-services--item bg contact-services" style="background-image: url('{{asset(''.section($page,'Section 2 Get An Estimate Background').'')}}')">
                        <div>

                            {!!section($page,'Section 2 Get An Estimate Content')!!}
                            {{-- <strong class="heading">Contact Us</strong>
                            <h3>Get An estimate</h3>
                            <p>Our expert team is ready to respond to your repair, service or new construction need. How can we help?</p>
                            <a href="{{url('contact-us')}}" class="btn btn--primary btn--white btn-contact-us">
                                <img src="{{asset('public/img/phone_icon_colored.png')}}" alt="Phone Icon">
                                Call Us
                            </a>
                            <a href="mailto:contactus@c-v-e.com" class="btn btn--primary btn--white btn-email-us">
                                <img src="{{asset('public/img/email_icon-colored.png')}}" alt="Email Icon">
                                Email Us
                            </a> --}}

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('front.layouts.sections.footer')
</section>