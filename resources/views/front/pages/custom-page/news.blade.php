<section class="page page--news">
    @include('front.layouts.sections.header')
    {{-- @include('front.pages.custom-page.sections.banner') --}}
    <main class="main-content">
        
         {{-- BANNER --}}
         <section class="section--sub-banner image-background">
            <img src="{{asset(''.section($page,'Banner Image').'')}}" alt="News and Events">
            <div class="wrapper">
                <div class="section--sub-banner__content">
                    <h1>{{section($page,'Title')}}</h1>
                </div>
            </div>
        </section>
         {{-- END OF BANNER --}}

         @php
            $news = \App\Models\News::orderBy('date','desc')->where('is_active',1)->get()
         @endphp

        <section class="section--list-of-events bg" style="background-image:url('{{asset('public/img/background/news-blogs_bg.jpg')}}');">
            <div class="wrapper">
                <div class="lastest-updates">
                    <strong class="heading">{{section($page,'Section 1 Left Title')}}</strong>
                    <div class="recent-post new clearfix">

                        @php
                            $first_news = $news->first();
                        @endphp

                        <div class="floatLeft image-background">
                            <img src="{{''.$first_news->banner_image.''}}" alt="Recent News">
                        </div>
                        <div class="floatRight">
                            <div class="news_text">

                                <h2>{{$first_news->name}}</h2>
                                <span class="category">{{$first_news->news_category->name}}</span>
                                <span class="date">{{  Carbon\Carbon::parse($first_news->date)->format('F d, Y') }}</span>

                                <p>{!! str_limit(strip_tags($first_news->content_1), 150) !!}</p>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p> --}}

                                <div class="text-right">
                                    <a href="{{url('news-details/'.$first_news->slug.'')}}" class="btn btn--view">learn more</a>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="list-of-news row">

                        @foreach ($news as $item)
                            
                        @if(!$loop->first)
                            <div class="col-md-6 list-of-news--item">
                                <a href="{{url('news-details/'.$item->slug.'')}}">
                                    <div class="relative">
                                        {{-- Dont Delete This --}}
                                        {{-- <img src="{{'public/img/empty-img.png'}}" alt="Empty Image"> --}}
                                        <img src="{{''.$item->banner_image.''}}" alt="Empty Image">
                                        <figure>
                                            <img src="{{''.$item->banner_image.''}}" alt="News">
                                            {{-- <img src="{{'public/img/news/news-1.jpg'}}" alt="News"> --}}
                                        </figure>
                                    </div>
                                    <div class="list-of-news--item_text">
                                        <h2>{{$item->name}}</h2>
                                        <span class="category">{{$item->news_category->name}}</span>
                                        <span class="date">{{  Carbon\Carbon::parse($item->date)->format('F d, Y') }}</span>
                                    </div>  
                                </a>
                            </div>
                        @endif

                        @endforeach
                        <!--
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-1.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Altus Power acquires five solar farms from Beltline Energy</h2>
                                    <span class="category">CBS News</span>
                                    <span class="date">September 8, 2020</span>
                                </div>  
                            </a>
                        </div>
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-2.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Wind and solar producer tops Exxon as most valuable U.S. energy company</h2>
                                    <span class="category">Union Tribune</span>
                                    <span class="date">May 1, 2020</span>
                                </div>  
                            </a>
                        </div>
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-3.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Altus Power acquires five solar farms from Beltline Energy</h2>
                                    <span class="category">CBS News</span>
                                    <span class="date">September 8, 2020</span>
                                </div>  
                            </a>
                        </div>
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-4.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Wind and solar producer tops Exxon as most valuable U.S. energy company</h2>
                                    <span class="category">Union Tribune</span>
                                    <span class="date">May 1, 2020</span>
                                </div>  
                            </a>
                        </div>
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-5.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Altus Power acquires five solar farms from Beltline Energy</h2>
                                    <span class="category">Union Tribune</span>
                                    <span class="date">September 8, 2020</span>
                                </div>  
                            </a>
                        </div>
                        <div class="col-md-6 list-of-news--item">
                            <a href="{{url('news-details')}}">
                                <div class="relative">
                                    {{-- Dont Delete This --}}
                                    <img src="{{'public/img/empty-img.png'}}" alt="Empty Image">
                                    <figure>
                                        <img src="{{'public/img/news/news-6.jpg'}}" alt="News">
                                    </figure>
                                </div>
                                <div class="list-of-news--item_text">
                                    <h2>Wind and solar producer tops Exxon as most valuable U.S. energy company</h2>
                                    <span class="category">Union Tribune</span>
                                    <span class="date">May 1, 2020</span>
                                </div>  
                            </a>
                        </div> 
                        -->
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    @include('front.layouts.sections.footer')
</section>