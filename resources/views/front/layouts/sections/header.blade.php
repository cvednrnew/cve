<header>

    {{-- FOR DESKTOP MENU  --}}
    <div class="main-nagivation-desktop">
        <div class="wrapper">
            <div class="row">
                <div class="col-5 logo-link">
                    <a href="{{url('/')}}" >
                        <img src="{{asset('public/img/logo.png')}}" alt="Logo" class="logo">
                        <img src="{{asset('public/img/text-logo.png')}}" alt="Text Logo" class="text-logo">
                    </a>
                </div>
                <div class="col-7 text-right menu">
                    <div class="">
                        <ul>
                            <li class="dropdown">
                                <a href="{{url('services')}}">Services</a>
                                <div class="dropdown-menu mega-menu">
                                    <ul class="sub-menu">
                                        @foreach (\App\Models\Service::where('is_active',1)->get() as $item)
                                            <li>
                                                <a class="nav-link" href="{{url('service-details/'.$item->slug)}}">
                                                    <span>{{$item->name}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <li {{-- class="dropdown" --}} >
                                <a href="{{url('projects')}}">Projects</a>
                                {{-- <div class="dropdown-menu mega-menu">
                                    <ul class="sub-menu">
                                        @foreach (\App\Models\Project::all() as $item)
                                            <li>
                                                <a class="nav-link" href="{{url('project-details/'.$item->slug)}}">
                                                    <span>{{$item->name}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div> --}}
                            </li>
                            <li><a href="{{url('renewable-energy')}}">Renewable Energy</a></li>
                            <li>
                            {{-- <li class="dropdown" > --}}
                                <a href="{{url('about-us')}}">About Us</a>
                                {{-- <div class="dropdown-menu mega-menu">
                                    <ul class="sub-menu">
                                        <li>
                                            <a class="nav-link" href="{{url('team')}}">
                                                <span>Team</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div> --}}
                            </li>
                            {{-- <li><a href="{{url('contact-us')}}" class="btn-contact">Contact Us</a></li> --}}
                            <li><a href="{{url('contact-us')}}">Contact Us</a></li>

                        </ul>
                    </div>
                    <div class="hamburger">
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- FOR MOBILE MENU  --}}
    <div class="main-navigation-mobile">
        <div class="main-navigation--bottom">
            <div class="wrapper">
                <div class="row align-items-center">
                    <div class="col-6 logo-link">
                        <a href="{{url('/')}}" >
                            <img src="{{asset('public/img/logo.png')}}" alt="Logo" class="logo">
                            <img src="{{asset('public/img/text-logo.png')}}" alt="Text Logo" class="text-logo">
                        </a>
                    </div>
                    <div class="col-6 text-right">
                        <a href="javascript:void(0)" class="hamburger">
                            <div></div>
                        </a>
                        <div class="main-menu">
                            <ul class="accordion">
                                <li class="has-dropdown">
                                    <a data-toggle="collapse" class="has-collapse" href="#services" >Services</a>
                                    <div class="sub-dropdown-menu collapse" id="services">
                                        <ul>
                                            @foreach (\App\Models\Service::where('is_active',1)->get() as $item)
                                                <li>
                                                    <a class="nav-link" href="{{url('service-details/'.$item->slug)}}">
                                                        <span>{{$item->name}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                            <li>
                                                <a class="nav-link" href="{{url('services')}}">
                                                    <span>View All</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li  {{-- class="has-dropdown" --}} >
                                    <a  href="{{url('projects')}}" {{-- data-toggle="collapse" class="has-collapse"  href="#projects"--}} >Projects</a>
                                    {{-- <div class="sub-dropdown-menu collapse" id="projects">
                                        <ul>
                                            @foreach (\App\Models\Project::all() as $item)
                                                <li>
                                                    <a class="nav-link" href="{{url('project-details/'.$item->slug.'')}}">
                                                        <span>{{$item->name}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                            <li>
                                                <a class="nav-link" href="{{url('projects')}}">
                                                    <span>View All</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div> --}}
                                </li>
                                <li><a href="{{url('renewable-energy')}}">Renewable Energy</a></li>
                                <li><a href="{{url('about-us')}}">About Us</a></li>
                                {{-- <li class="has-dropdown">
                                    <a data-toggle="collapse" class="has-collapse" href="#about-us" >About</a>
                                    <div class="sub-dropdown-menu collapse" id="about-us">
                                        <ul>
                                            <li>
                                                <a class="nav-link" href="{{url('team')}}">
                                                    <span>Team</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="nav-link" href="{{url('about-us')}}">
                                                    <span>About Us</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li> --}}
                                {{-- <li><a href="{{url('contact-us')}}" class="btn-contact">Contact Us</a></li> --}}
                                <li><a href="{{url('contact-us')}}">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
