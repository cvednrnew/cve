<footer class="footer bg" style="background-image: url('{{url('public/img/contact-bg.jpg')}}')">
   <div class="footer__menu">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class=" text-center">
                        <ul>
                            <li>
                                <a href="{{url('services')}}">Services</a>
                            </li>
                            <li>
                                <a href="{{url('projects')}}">Projects</a>
                            </li>
                            <li>
                                <a href="{{url('renewable-energy')}}">Renewable Energy</a>
                            </li>
                            {{-- <li>
                                <a href="{{url('news')}}">News</a>
                            </li> --}}
                            <li>
                                <a href="{{url('about-us')}}">About</a>
                            </li>
                            <li>
                                <a href="{{url('contact-us')}}">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
   </div>

    <div class="footer__contact">
        <div class="wrapper">
            <div class="row">
                
                <div class="col-lg-4 col-md-3 col-sm-12 text-center">
                    {!!settings('SS0010')!!}
                    {{-- <img src="{{asset('public/img/contact_logo.png')}}" alt="Logo" class="logo">
                    <img src="{{asset('public/img/contact_text-logo.png')}}" alt="Chula Vista Elecric Co." class="text-logo"> --}}
                </div>

                <div class="col-lg-5 col-md-5 col-sm-6 info">
                    {!!settings('SS0011')!!}
{{-- 
                    <p class="description">CVE exists to empower organizations with reliable and advanced electrical systems and networks.</p>
                    <p class="contact-number">619-420-4500</p>
                    <p class="email-address">contactus@c-v-e.com</p> --}}

                    <div class="social-icons">
                        <ul>

                            @if (settings('SS0013') == '' || settings('SS0013') == 'err')
                            @else
                                <li>
                                    <a href="{{settings('SS0013')}}">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                            @endif
                            
                            @if (settings('SS0015') == '' || settings('SS0015') == 'err')
                            @else
                                <li>
                                    <a href="{{settings('SS0015')}}">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                            @endif

                            @if (settings('SS0014') == '' || settings('SS0014') == 'err')
                            @else
                                <li>
                                    <a href="{{settings('SS0014')}}">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                            @endif

                            @if (settings('SS0016') == '' || settings('SS0016') == 'err')
                            @else
                                <li>
                                    <a href="{{settings('SS0016')}}">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            @endif

                            @if (settings('SS0017') == '' || settings('SS0017') == 'err')
                            @else
                                <li>
                                    <a href="{{settings('SS0017')}}">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                            @endif

                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="form">
                        <h2>Contact Us</h2>
                        {{  Form::open([
                            'method' => 'POST',
                            'id' => 'create-contact',
                            'route' => ['contact.store'],
                            'class' => '',
                            ])
                        }}

                            <div class="control">
                                <input type="text" placeholder="First Name" name="firstname" required>
                                @if($errors->has('firstname'))
                                <span class="help-block animation-slideDown">{{ $errors->first('firstname') }}</span>
                                @endif
                            </div>
                            <div class="control">
                                <input type="text" placeholder="Last Name" name="lastname" required>
                                @if($errors->has('lastname'))
                                <span class="help-block animation-slideDown">{{ $errors->first('lastname') }}</span>
                                @endif
                            </div>
                            <div class="control">
                                <input type="email" placeholder="Email Address" name="email" required>
                                @if($errors->has('email'))
                                <span class="help-block animation-slideDown">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="control">
                                <textarea placeholder="Message" name="message" required></textarea>
                                @if($errors->has('message'))
                                <span class="help-block animation-slideDown">{{ $errors->first('message') }}</span>
                                @endif
                            </div>
                            <div class="control">
                                <input type="submit">
                            </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer__copyright">
    <div class="wrapper">
        <div class="row text-center">
            <div class="col-md-12">
                <p>
                    {!!settings('SS0007')!!}
                    {{-- COPYRIGHT ALL RESEARVED @ 2021 CHULA VISTA ELECTRIC CO. | WEB DESIGN BY DOG AND ROOSTER, INC. --}}
                </p>
            </div>
        </div>
    </div>
</div>