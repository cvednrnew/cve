$('.banner__slick').slick({
    dots: false,
    infinite: true,
    speed: 800,
    slidesToShow: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    fade: true,
    cssEase: 'linear',
    arrows: true,
});

// Hero Slider
$(function() {
  // Slick slider for prev/next thumbnails images
  $('.hero-slider').slick({
        dots: true,
        slidesToShow: 1,
        autoplay: false,
        prevArrow: $('.hero-prev'),
        nextArrow: $('.hero-next'),
        arrows: true,
        cssEase: 'linear',
    });
    setTimeout(function() {
        $('.arrow-prev_img').prepend('<div class="prevSlickImg slick-thumb-nav"><img src="" class="img-responsive"></div>');
        $('.arrow-next_img').append('<div class="nextSlickImg slick-thumb-nav"><img src="" class="img-responsive"></div>');
        get_prev_slick_img();
        get_next_slick_img();
    }, 500);

    $('.hero-prev').on('click', function() {
        get_prev_slick_img();
    });
    $('.hero-next').on('click', function() {
        get_next_slick_img();
    });
    $('.hero-slider').on('swipe', function(event, slick, direction) {
        if (direction == 'left') {
            get_prev_slick_img();
        }
        else {
        get_next_slick_img();
        }
    });
    $('.slick-dots').on('click', 'li button', function() {
        var li_no = $(this).parent('li').index();
        if ($(this).parent('li').index() > li_no) {
            get_prev_slick_img()
        }
        else {
            get_next_slick_img()
        }
  });
    function get_prev_slick_img() {
        // For prev img
        var prev_slick_img = $('.slick-current').prev().find('img').attr('src');
        // alert(prev_slick_img);
        $('.prevSlickImg img').attr('src', prev_slick_img);
        $('.prevSlickImg').css('background-image', 'url(' + prev_slick_img + ')');
        // For next img
        var prev_next_slick_img = $('.slick-current').next().find('img').attr('src');
        $('.nextSlickImg img').attr('src', prev_next_slick_img);
        $('.nextSlickImg').css('background-image', 'url(' + prev_next_slick_img + ')');
    }

    function get_next_slick_img() {
        // For next img
        var next_slick_img = $('.slick-current').next().find('img').attr('src');
        $('.nextSlickImg img').attr('src', next_slick_img);
        $('.nextSlickImg').css('background-image', 'url(' + next_slick_img + ')');
        // For prev img
        var next_prev_slick_img = $('.slick-current').prev().find('img').attr('src');
        $('.prevSlickImg img').attr('src', next_prev_slick_img);
        $('.prevSlickImg').css('background-image', 'url(' + next_prev_slick_img + ')');
    }
})


// Testimonial
var $status = $('.slidercount');
var $slickSlider = $('.client-testimonail-slider');

$slickSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + '/' + slick.slideCount);
});

$slickSlider.slick({
    autoplay: true,
    dots: false,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
}); 

$('.project--slider').slick({
    dots: false,
    infinite: true,
    speed: 800,
    slidesToShow: 2,
    prevArrow: $('.project-prev'),
    nextArrow: $('.project-next'),
    responsive: [
        {
          breakpoint: 770,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
      ]
});


$('.related-project--slider').slick({
  dots: false,
  infinite: true,
  speed: 800,
  slidesToShow: 3,
  slidesToScroll: 3,
  prevArrow: $('.project-prev'),
  nextArrow: $('.project-next'),
  responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
});

// $('.team-member-slider').slick({
//   dots: false,
//   infinite: true,
//   speed: 800,
//   slidesToShow: 4,
//   slidesToScroll: 2,
//   prevArrow: $('.project-prev'),
//   nextArrow: $('.project-next'),
//   responsive: [
//       {
//         breakpoint: 770,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//           infinite: true,
//           dots: false
//         }
//       },
//     ]
// });

// Testimonial
var $status = $('.slidercount');
var $slickSlider = $('.team-member-slider');

$slickSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + '/' + slick.slideCount);
});

$slickSlider.slick({
    dots: false,
    infinite: true,
    speed: 800,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: $('.team-prev'),
    nextArrow: $('.team-next'),
    responsive: [
        {
          breakpoint: 1110,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
            breakpoint: 500,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          },
      ]
});

$('.section--timeline-slider').slick({
  dots: false,
  infinite: true,
  speed: 800,
  slidesToShow: 1,
  adaptiveHeight: true,
  prevArrow: jQuery('.timeline-prev'),
  nextArrow: jQuery('.timeline-next'),
  responsive: [
      {
      breakpoint: 770,
      settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
      }
      },
  ]
});



