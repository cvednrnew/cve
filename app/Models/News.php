<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class News
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'date',
        'link',
        'category_id',
        'slug',
        'banner_image',
        'file',
        'content_1',
        'content_2',
        'is_active',
    ];

    public function news_category()
    {
       return $this->belongsTo('\App\Models\NewsCategory','category_id');
    }
}