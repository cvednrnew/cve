<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewsCategory
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class NewsCategory extends Model
{
    use SoftDeletes;

    protected $table = 'newscategories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        // 'banner_image',
        // 'file',
        // 'content',
        // 'is_active',
    ];
}