<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Project
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Project extends Model
{
    use SoftDeletes;

    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'banner_image',
        'file',
        'content',
        'category_service_arr',
        'seo_meta_id',
        'is_active',
        'order',
    ];

    public final function seoMeta()
    {
        return $this->belongsTo(SeoMeta::class);
    }

    public final function gallery()
    {
        return $this->hasMany(ProjectGallery::class)->orderBy('order','asc');
    }

}