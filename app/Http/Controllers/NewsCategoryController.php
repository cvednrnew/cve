<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsCategory;
use App\Repositories\NewsCategoryRepository;

/**
 * Class NewsCategoryController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class NewsCategoryController extends Controller
{
    /**
     * NewsCategory model instance.
     *
     * @var NewsCategory
     */
    private $newscategory_model;

    /**
     * NewsCategoryRepository repository instance.
     *
     * @var NewsCategoryRepository
     */
    private $newscategory_repository;

    /**
     * Create a new controller instance.
     *
     * @param NewsCategory $newscategory_model
     * @param NewsCategoryRepository $newscategory_repository
     */
    public function __construct(NewsCategory $newscategory_model, NewsCategoryRepository $newscategory_repository)
    {
        /*
         * Model namespace
         * using $this->newscategory_model can also access $this->newscategory_model->where('id', 1)->get();
         * */
        $this->newscategory_model = $newscategory_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of newscategories with other data (related tables).
         * */
        $this->newscategory_repository = $newscategory_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read NewsCategory')) {
            abort('401', '401');
        }

        $newscategories = $this->newscategory_model->get();

        return view('admin.pages.newscategory.index', compact('newscategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create NewsCategory')) {
            abort('401', '401');
        }

        return view('admin.pages.newscategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create NewsCategory')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:newscategories,name,NULL,id,deleted_at,NULL',
            // 'slug' => 'required|unique:newscategories,slug,NULL,id,deleted_at,NULL',
            // 'content' => 'required',
            // 'banner_image' => 'mimes:jpg,jpeg,png',
            // 'file' => 'mimes:docx,doc,pdf',
        ]);

        $input = $request->all();
        // $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $newscategory = $this->newscategory_model->create($input);

        // if ($request->hasFile('banner_image')) {
        //     $file_upload_path = $this->newscategory_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'newscategory_images');
        //     $newscategory->fill(['banner_image' => $file_upload_path])->save();
        // }
        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->newscategory_repository->uploadFile($request->file('file'), /*'file'*/null, 'newscategory_files');
        //     $newscategory->fill(['file' => $file_upload_path])->save();
        // }

        return redirect()->route('admin.newscategories.index')->with('flash_message', [
            'title' => '',
            'message' => 'NewsCategory ' . $newscategory->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read NewsCategory')) {
            abort('401', '401');
        }

        $newscategory = $this->newscategory_model->findOrFail($id);

        return view('admin.pages.newscategory.show', compact('newscategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update NewsCategory')) {
            abort('401', '401');
        }

        $newscategory = $this->newscategory_model->findOrFail($id);

        return view('admin.pages.newscategory.edit', compact('newscategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update NewsCategory')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:newscategories,name,' . $id . ',id,deleted_at,NULL',
            // 'slug' => 'required|unique:newscategories,slug,' . $id . ',id,deleted_at,NULL',
            // 'content' => 'required',
            // 'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            // 'file' => 'required_if:remove_file,==,1|mimes:docx,doc,pdf',
        ]);

        $newscategory = $this->newscategory_model->findOrFail($id);
        $input = $request->all();
        // $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        // if ($request->hasFile('banner_image')) {
        //     $file_upload_path = $this->newscategory_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'newscategory_images');
        //     $input['banner_image'] = $file_upload_path;
        // }
        // if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
        //     $input['banner_image'] = '';
        // }

        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->newscategory_repository->uploadFile($request->file('file'), /*'file'*/null, 'newscategory_files');
        //     $input['file'] = $file_upload_path;
        // }
        // if ($request->has('remove_file') && $request->get('remove_file')) {
        //     $input['file'] = '';
        // }

        $newscategory->fill($input)->save();

        return redirect()->route('admin.newscategories.index')->with('flash_message', [
            'title' => '',
            'message' => 'NewsCategory ' . $newscategory->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete NewsCategory')) {
            abort('401', '401');
        }

        $newscategory = $this->newscategory_model->findOrFail($id);
        $newscategory->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'NewsCategory successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
