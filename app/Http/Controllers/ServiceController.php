<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Repositories\SeoMetaRepository;
use App\Repositories\ServiceRepository;

/**
 * Class ServiceController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ServiceController extends Controller
{
    /**
     * Service model instance.
     *
     * @var Service
     */
    private $service_model;

    /**
     * ServiceRepository repository instance.
     *
     * @var ServiceRepository
     */
    private $service_repository;

    /**
     * Create a new controller instance.
     *
     * @param Service $service_model
     * @param ServiceRepository $service_repository
     * @param SeoMetaRepository $seo_meta_repository
     */
    public function __construct(Service $service_model, ServiceRepository $service_repository, SeoMetaRepository $seo_meta_repository)
    {
        /*
         * Model namespace
         * using $this->service_model can also access $this->service_model->where('id', 1)->get();
         * */
        $this->service_model = $service_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of services with other data (related tables).
         * */
        $this->service_repository = $service_repository;
        $this->seo_meta_repository = $seo_meta_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Service')) {
            abort('401', '401');
        }

        $services = $this->service_model->get();

        return view('admin.pages.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Service')) {
            abort('401', '401');
        }

        return view('admin.pages.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Service')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:services,name,NULL,id,deleted_at,NULL',
            // 'slug' => 'required|unique:services,slug,NULL,id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'mimes:jpg,jpeg,png',
            'file' => 'mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        $service = $this->service_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->service_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'service_images');
            $service->fill(['banner_image' => $file_upload_path])->save();
        }
        if ($request->hasFile('file')) {
            $file_upload_path = $this->service_repository->uploadFile($request->file('file'), /*'file'*/null, 'service_files');
            $service->fill(['file' => $file_upload_path])->save();
        }

        return redirect()->route('admin.services.index')->with('flash_message', [
            'title' => '',
            'message' => 'Service ' . $service->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read Service')) {
            abort('401', '401');
        }

        $service = $this->service_model->findOrFail($id);

        return view('admin.pages.service.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Service')) {
            abort('401', '401');
        }

        $service = $this->service_model->findOrFail($id);

        return view('admin.pages.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Service')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:services,name,' . $id . ',id,deleted_at,NULL',
            // 'slug' => 'required|unique:services,slug,' . $id . ',id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            'file' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
        ]);

        $service = $this->service_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->service_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'service_images');
            $input['banner_image'] = $file_upload_path;
        }
        if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
            $input['banner_image'] = '';
        }

        if ($request->hasFile('file')) {
            $file_upload_path = $this->service_repository->uploadFile($request->file('file'), /*'file'*/null, 'service_files');
            $input['file'] = $file_upload_path;
        }
        if ($request->has('remove_file') && $request->get('remove_file')) {
            $input['file'] = '';
        }

        $service->fill($input)->save();

        return redirect()->route('admin.services.index')->with('flash_message', [
            'title' => '',
            'message' => 'Service ' . $service->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Service')) {
            abort('401', '401');
        }

        $service = $this->service_model->findOrFail($id);
        $service->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Service successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
