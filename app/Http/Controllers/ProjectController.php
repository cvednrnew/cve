<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectGallery;
use App\Repositories\ProjectRepository;
use App\Repositories\SeoMetaRepository;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ProjectController extends Controller
{
    /**
     * Project model instance.
     *
     * @var Project
     */
    private $project_model;

    /**
     * ProjectRepository repository instance.
     *
     * @var ProjectRepository
     */
    private $project_repository;

    /**
     * Create a new controller instance.
     *
     * @param Project $project_model
     * @param SeoMetaRepository $seo_meta_repository
     * @param ProjectRepository $project_repository
     */
    public function __construct(Project $project_model, ProjectRepository $project_repository, SeoMetaRepository $seo_meta_repository)
    {
        /*
         * Model namespace
         * using $this->project_model can also access $this->project_model->where('id', 1)->get();
         * */
        $this->project_model = $project_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of projects with other data (related tables).
         * */
        $this->seo_meta_repository = $seo_meta_repository;
        $this->project_repository = $project_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Project')) {
            abort('401', '401');
        }

        $projects = $this->project_model->get();

        return view('admin.pages.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Project')) {
            abort('401', '401');
        }

        return view('admin.pages.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Project')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:projects,name,NULL,id,deleted_at,NULL',
            // 'slug' => 'required|unique:projects,slug,NULL,id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'mimes:jpg,jpeg,png',
            'incrementPhoto.*' => 'mimes:jpg,jpeg,png,gif',
            // 'file' => 'mimes:docx,doc,pdf',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        $project = $this->project_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->project_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'project_images');
            $project->fill(['banner_image' => $file_upload_path])->save();
        }

        if ($request->hasFile('incrementPhoto')) {
            foreach ($request->file('incrementPhoto') as $item) {

                $file_upload_path = $this->project_repository->uploadFileGallery($item, /*'images'*/null, 'project_image_gallery');

                $image_gallery = new \App\Models\ProjectGallery();
                $image_gallery->project_id = $project->id;
                $image_gallery->image = $file_upload_path;
                $image_gallery->order = 0;
                $image_gallery->save();

                // $product->fill(['image' => $file_upload_path])->save();
            }
        }
        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->project_repository->uploadFile($request->file('file'), /*'file'*/null, 'project_files');
        //     $project->fill(['file' => $file_upload_path])->save();
        // }

        return redirect()->route('admin.projects.index')->with('flash_message', [
            'title' => '',
            'message' => 'Project ' . $project->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read Project')) {
            abort('401', '401');
        }

        $project = $this->project_model->findOrFail($id);

        return view('admin.pages.project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Project')) {
            abort('401', '401');
        }

        $project = $this->project_model->findOrFail($id);

        return view('admin.pages.project.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Project')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:projects,name,' . $id . ',id,deleted_at,NULL',
            // 'slug' => 'required|unique:projects,slug,' . $id . ',id,deleted_at,NULL',
            'content' => 'required',
            'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            // 'file' => 'required_if:remove_file,==,1|mimes:docx,doc,pdf',
        ]);

        $project = $this->project_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $input['category_service_arr'] = (json_encode($request->service_categ) == 'null' ? '' : json_encode($request->service_categ));

        /* seo meta */
        $input['seo_meta_id'] = isset($input['seo_meta_id']) ? $input['seo_meta_id'] : 0;
        $seo_inputs = $request->only(['meta_title', 'meta_keywords', 'meta_description', 'seo_meta_id']);
        $seo_meta = $this->seo_meta_repository->updateOrCreate($seo_inputs);
        $input['seo_meta_id'] = $seo_meta->id;
        /* seo meta */

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->project_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'project_images');
            $input['banner_image'] = $file_upload_path;
        }
        if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
            $input['banner_image'] = '';
        }

        if(isset($request->image_id))
        {
            foreach ($request->image_fix_current as $posti => $images_fixes)
            {
                $get_current = explode('_',$images_fixes);

                ProjectGallery::where('id',$get_current[1])
                    ->update([
                        'order' => $request->image_fixed_pos[$posti]
                        ]);
            }
        }

        if($request->has('remove_image_gallery'))
        {
            foreach($request->get('remove_image_gallery') as $remo)
            {
                \App\Models\ProjectGallery::where('id', $remo)->delete();
            }
        }

        if ($request->hasFile('incrementPhoto')) {
            foreach ($request->file('incrementPhoto') as $item) {

                $file_upload_path = $this->project_repository->uploadFileGallery($item, /*'images'*/null, 'project_image_gallery');

                $image_gallery = new \App\Models\ProjectGallery();
                $image_gallery->project_id = $project->id;
                $image_gallery->image = $file_upload_path;
                $image_gallery->order = 0;
                $image_gallery->save();

                // $product->fill(['image' => $file_upload_path])->save();
            }
        }


        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->project_repository->uploadFile($request->file('file'), /*'file'*/null, 'project_files');
        //     $input['file'] = $file_upload_path;
        // }
        // if ($request->has('remove_file') && $request->get('remove_file')) {
        //     $input['file'] = '';
        // }

        $project->fill($input)->save();

        return redirect()->route('admin.projects.index')->with('flash_message', [
            'title' => '',
            'message' => 'Project ' . $project->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Project')) {
            abort('401', '401');
        }

        $project = $this->project_model->findOrFail($id);
        $project->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Project successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
