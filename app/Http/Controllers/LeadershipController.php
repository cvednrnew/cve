<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leadership;
use App\Repositories\LeadershipRepository;

/**
 * Class LeadershipController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class LeadershipController extends Controller
{
    /**
     * Leadership model instance.
     *
     * @var Leadership
     */
    private $leadership_model;

    /**
     * LeadershipRepository repository instance.
     *
     * @var LeadershipRepository
     */
    private $leadership_repository;

    /**
     * Create a new controller instance.
     *
     * @param Leadership $leadership_model
     * @param LeadershipRepository $leadership_repository
     */
    public function __construct(Leadership $leadership_model, LeadershipRepository $leadership_repository)
    {
        /*
         * Model namespace
         * using $this->leadership_model can also access $this->leadership_model->where('id', 1)->get();
         * */
        $this->leadership_model = $leadership_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of leaderships with other data (related tables).
         * */
        $this->leadership_repository = $leadership_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Leadership')) {
            abort('401', '401');
        }

        $leaderships = $this->leadership_model->get();

        return view('admin.pages.leadership.index', compact('leaderships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Leadership')) {
            abort('401', '401');
        }

        return view('admin.pages.leadership.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Leadership')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required',
            // 'slug' => 'required|unique:leaderships,slug,NULL,id,deleted_at,NULL',
            'position' => 'required',
            'bio' => 'required',
            'banner_image' => 'mimes:jpg,jpeg,png',
            // 'file' => 'mimes:docx,doc,pdf',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $leadership = $this->leadership_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->leadership_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'leadership_images');
            $leadership->fill(['banner_image' => $file_upload_path])->save();
        }

        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->leadership_repository->uploadFile($request->file('file'), /*'file'*/null, 'leadership_files');
        //     $leadership->fill(['file' => $file_upload_path])->save();
        // }

        return redirect()->route('admin.leaderships.index')->with('flash_message', [
            'title' => '',
            'message' => 'Leadership ' . $leadership->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read Leadership')) {
            abort('401', '401');
        }

        $leadership = $this->leadership_model->findOrFail($id);

        return view('admin.pages.leadership.show', compact('leadership'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Leadership')) {
            abort('401', '401');
        }

        $leadership = $this->leadership_model->findOrFail($id);

        return view('admin.pages.leadership.edit', compact('leadership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Leadership')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required',
            'bio' => 'required',
            'position' => 'required',
            'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            // 'file' => 'required_if:remove_file,==,1|mimes:docx,doc,pdf',
        ]);

        $leadership = $this->leadership_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->leadership_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'leadership_images');
            $input['banner_image'] = $file_upload_path;
        }
        if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
            $input['banner_image'] = '';
        }

        if ($request->hasFile('file')) {
            $file_upload_path = $this->leadership_repository->uploadFile($request->file('file'), /*'file'*/null, 'leadership_files');
            $input['file'] = $file_upload_path;
        }
        if ($request->has('remove_file') && $request->get('remove_file')) {
            $input['file'] = '';
        }

        $leadership->fill($input)->save();

        return redirect()->route('admin.leaderships.index')->with('flash_message', [
            'title' => '',
            'message' => 'Leadership ' . $leadership->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Leadership')) {
            abort('401', '401');
        }

        $leadership = $this->leadership_model->findOrFail($id);
        $leadership->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Leadership successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
