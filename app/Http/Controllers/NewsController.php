<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Repositories\NewsRepository;

/**
 * Class NewsController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class NewsController extends Controller
{
    /**
     * News model instance.
     *
     * @var News
     */
    private $news_model;

    /**
     * NewsRepository repository instance.
     *
     * @var NewsRepository
     */
    private $news_repository;

    /**
     * Create a new controller instance.
     *
     * @param News $news_model
     * @param NewsRepository $news_repository
     */
    public function __construct(News $news_model, NewsRepository $news_repository)
    {
        /*
         * Model namespace
         * using $this->news_model can also access $this->news_model->where('id', 1)->get();
         * */
        $this->news_model = $news_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of news with other data (related tables).
         * */
        $this->news_repository = $news_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read News')) {
            abort('401', '401');
        }

        $news = $this->news_model->get();

        return view('admin.pages.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create News')) {
            abort('401', '401');
        }

        return view('admin.pages.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create News')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:news,name,NULL,id,deleted_at,NULL',
            // 'slug' => 'required|unique:news,slug,NULL,id,deleted_at,NULL',
            'content_1' => 'required',
            'banner_image' => 'mimes:jpg,jpeg,png',
            // 'file' => 'mimes:docx,doc,pdf',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $news = $this->news_model->create($input);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->news_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'news_images');
            $news->fill(['banner_image' => $file_upload_path])->save();
        }
        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->news_repository->uploadFile($request->file('file'), /*'file'*/null, 'news_files');
        //     $news->fill(['file' => $file_upload_path])->save();
        // }

        return redirect()->route('admin.news.index')->with('flash_message', [
            'title' => '',
            'message' => 'News ' . $news->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read News')) {
            abort('401', '401');
        }

        $news = $this->news_model->findOrFail($id);

        return view('admin.pages.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update News')) {
            abort('401', '401');
        }

        $news = $this->news_model->findOrFail($id);

        return view('admin.pages.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update News')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required',
            // 'slug' => 'required|unique:news,slug,' . $id . ',id,deleted_at,NULL',
            'content_1' => 'required',
            'banner_image' => 'required_if:remove_banner_image,==,1|mimes:jpg,jpeg,png',
            // 'file' => 'required_if:remove_file,==,1|mimes:docx,doc,pdf',
        ]);

        $news = $this->news_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        if ($request->hasFile('banner_image')) {
            $file_upload_path = $this->news_repository->uploadFile($request->file('banner_image'), /*'banner_image'*/null, 'news_images');
            $input['banner_image'] = $file_upload_path;
        }
        if ($request->has('remove_banner_image') && $request->get('remove_banner_image')) {
            $input['banner_image'] = '';
        }

        // if ($request->hasFile('file')) {
        //     $file_upload_path = $this->news_repository->uploadFile($request->file('file'), /*'file'*/null, 'news_files');
        //     $input['file'] = $file_upload_path;
        // }
        // if ($request->has('remove_file') && $request->get('remove_file')) {
        //     $input['file'] = '';
        // }

        $news->fill($input)->save();

        return redirect()->route('admin.news.index')->with('flash_message', [
            'title' => '',
            'message' => 'News ' . $news->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete News')) {
            abort('401', '401');
        }

        $news = $this->news_model->findOrFail($id);
        $news->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'News successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
