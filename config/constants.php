<?php

return [
    "placeholder_image" => "public/images/placeholders/placeholder.jpg",
    "no_reply_email" => "no-reply@dogandrooster.com",
    "dnr_bcc" => ["programmer1@dogandrooster.com","frontend@dogandrooster.com"]
];