<?php

/* dashboard routes */
Route::get('/', function () {
    return redirect('/admin/dashboard');
});

/* dashboard */
Route::get('/dashboard', [
    'uses' => '\App\Http\Controllers\AdminDashboardController@index',
    'as' => 'admin.dashboard',
]);
/* dashboard */

/* users */
Route::get('/users/draw',
    ['as' => 'admin.users.draw',
        'uses' => '\App\Http\Controllers\UserController@draw']
);

Route::resource('/users', 'UserController', [
    'as' => 'admin'
]);

Route::delete('/users/{id}/delete',
    ['as' => 'admin.users.delete',
        'uses' => '\App\Http\Controllers\UserController@destroy']
);
/* users */

/* roles */
Route::resource('/roles', 'RoleController', [
    'as' => 'admin'
]);

Route::delete('/roles/{id}/delete',
    ['as' => 'admin.roles.delete',
        'uses' => '\App\Http\Controllers\RoleController@destroy']
);
/* roles */

/* permissions */
Route::resource('/permissions', 'PermissionController', [
    'as' => 'admin'
]);

Route::delete('/permissions/{id}/delete',
    ['as' => 'admin.permissions.delete',
        'uses' => '\App\Http\Controllers\PermissionController@destroy']
);
/* permissions */

/* permission groups */
Route::resource('/permission_groups', 'PermissionGroupController', [
    'as' => 'admin'
]);

Route::delete('/permission_groups/{id}/delete',
    ['as' => 'admin.permission_groups.delete',
        'uses' => '\App\Http\Controllers\PermissionGroupController@destroy']
);
/* permission groups */

/* system settings */
Route::resource('/system_settings', 'SystemSettingController', [
    'as' => 'admin',
]);

Route::delete('/system_settings/{id}/delete',
    ['as' => 'admin.system_settings.delete',
        'uses' => '\App\Http\Controllers\SystemSettingController@destroy']
);
/* system settings */

/* posts */
Route::resource('/posts', 'PostController', [
    'as' => 'admin'
]);

Route::delete('/posts/{id}/delete',
    ['as' => 'admin.posts.delete',
        'uses' => '\App\Http\Controllers\PostController@destroy']
);
/* posts */

/* pages */
Route::resource('/pages', 'PageController', [
    'as' => 'admin'
]);

Route::delete('/pages/{id}/delete',
    ['as' => 'admin.pages.delete',
        'uses' => '\App\Http\Controllers\PageController@destroy']
);
/* pages */

/* ckeditor image upload */
Route::post('/ckeditor_image_upload',
    ['as' => 'admin.ckeditor_image_upload',
        'uses' => '\App\Http\Controllers\PageController@ckEditorImageUpload']
);
/* ckeditor image upload */

Route::post('/upload', '\App\Http\Controllers\PageController@upload')->name('admin.upload');

/* home_slides */
Route::resource('/home_slides', 'HomeSlideController', [
    'as' => 'admin'
]);

Route::delete('/home_slides/{id}/delete',
    ['as' => 'admin.home_slides.delete',
        'uses' => '\App\Http\Controllers\HomeSlideController@destroy']
);
/* home_slides */

/* news_press */
Route::resource('/news_press', 'NewsPressController', [
    'as' => 'admin'
]);

Route::delete('/news_press/{id}/delete',
    ['as' => 'admin.news_press.delete',
        'uses' => '\App\Http\Controllers\NewsPressController@destroy']
);
/* news_press */

/* projct_case_study */
Route::resource('/projct_case_study', 'ProjectCaseController', [
    'as' => 'admin'
]);

Route::delete('/projct_case_study/{id}/delete',
    ['as' => 'admin.projct_case_study.delete',
        'uses' => '\App\Http\Controllers\ProjectCaseController@destroy']
);
/* projct_case_study */

/* ser_sol */
Route::resource('/ser_sol', 'ServiceSolutionController', [
    'as' => 'admin'
]);

Route::delete('/ser_sol/{id}/delete',
    ['as' => 'admin.ser_sol.delete',
        'uses' => '\App\Http\Controllers\ServiceSolutionController@destroy']
);
/* ser_sol */

/* products */
Route::resource('/products', 'ProductController', [
    'as' => 'admin'
]);

Route::delete('/products/{id}/delete',
    ['as' => 'admin.products.delete',
        'uses' => '\App\Http\Controllers\ProductController@destroy']
);
/* products */

/* product categories */
Route::resource('/product_categories', 'ProductCategoryController', [
    'as' => 'admin'
]);

Route::delete('/product_categories/{id}/delete',
    ['as' => 'admin.product_categories.delete',
        'uses' => '\App\Http\Controllers\ProductCategoryController@destroy']
);
/* product categories */

/* contacts */
Route::resource('/contacts', 'ContactController', [
    'as' => 'admin'
]);

Route::delete('/contacts/{id}/delete',
    ['as' => 'admin.contacts.delete',
        'uses' => '\App\Http\Controllers\ContactController@destroy']
);
/* contacts */

/* presses */


/*Route::resource('/faqs', 'FAQController', [
    'as' => 'admin'
]);

*/


/* projects */
        Route::resource('/projects', 'ProjectController', [
            'as' => 'admin'
        ]);

Route::delete('/projects/{id}/delete',
            ['as' => 'admin.projects.delete',
                'uses' => '\App\Http\Controllers\ProjectController@destroy']
        );
        /* projects */

/* services */
        Route::resource('/services', 'ServiceController', [
            'as' => 'admin'
        ]);

Route::delete('/services/{id}/delete',
            ['as' => 'admin.services.delete',
                'uses' => '\App\Http\Controllers\ServiceController@destroy']
        );
        /* services */

/* news */
        Route::resource('/news', 'NewsController', [
            'as' => 'admin'
        ]);

Route::delete('/news/{id}/delete',
            ['as' => 'admin.news.delete',
                'uses' => '\App\Http\Controllers\NewsController@destroy']
        );
        /* news */

/* leaderships */
        Route::resource('/leaderships', 'LeadershipController', [
            'as' => 'admin'
        ]);

Route::delete('/leaderships/{id}/delete',
            ['as' => 'admin.leaderships.delete',
                'uses' => '\App\Http\Controllers\LeadershipController@destroy']
        );
        /* leaderships */

/* newscategories */
        Route::resource('/newscategories', 'NewsCategoryController', [
            'as' => 'admin'
        ]);

Route::delete('/newscategories/{id}/delete',
            ['as' => 'admin.newscategories.delete',
                'uses' => '\App\Http\Controllers\NewsCategoryController@destroy']
        );
        /* newscategories */